#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include "greet.h"

/*! \file main.cpp */
//! Function to call in case of invalid or missing argument
/*!
    \return amount of prints - int
*/
int invalidArg()
{
    std::cerr << "Invalid arguments!" << std::endl;
    std::cout << "Enter valid integer input: ";
    int val;
    std::cin >> val;
    while (!std::cin)
    {
        std::cin.clear();
        std::cin.ignore(999, '\n');
        std::cout << "Enter valid integer input: ";
        std::cin >> val;
    }
    return val;
}

//! Function to print object
/*!
    // Creates val amount of objects inside a vector, and prints them out to the console.
    \param amount of prints - int
*/
void printObject(int val)
{
    std::vector<HelloGreeter> hellogreeter;
    for (int i = 0; i < val; i++)
    {
        HelloGreeter hey;
        hellogreeter.push_back(hey);
    }
    std::vector<HelloGreeter>::iterator it;
    for (it = hellogreeter.begin(); it != hellogreeter.end(); it++)
    {
        std::cout << it->sayHello() << std::endl;
    }
}

int main(int argc, char* argv[])
{
    int i;
    int val;
    try
    {
        if (argc == 2)
        {
            i = std::stoi(argv[1]);
            printObject(i);
            return 0;
        }
        else
        {
            val = invalidArg();
            printObject(val);
            return 0;
        }
    }
    catch (std::exception const &exc)
    {
        val = invalidArg();
        printObject(val);
        return 0;
    }
}
