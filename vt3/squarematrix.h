#ifndef SQUAREMATRIX_H
#define SQUAREMATRIX_H
#include <iostream>
#include <vector>
#include "intelement.h"

/** \file squarematrix.h */
//! Class to create a SquareMatrix
class SquareMatrix
{
    public:
        //! Default Constructor.
        SquareMatrix();
        //! Constructor
        /*!
        \param n_i - int
        \param elements - vector<vector<IntElement>>&
        */
        SquareMatrix(int i_n, const std::vector<std::vector<IntElement>>& elements);
        //! Copy constructor
        /*!
        \param m - const SquareMatrix&
        */
        SquareMatrix(const SquareMatrix& m);
        //! Destructor.
        virtual ~SquareMatrix();
        //! Transpose matrix
        /*!
        \return SquareMatrix
        */
        SquareMatrix transpose();
        //! Addition operator
        /*!
        \param m - const SquareMatrix&
        \return SquareMatrix&
        */
        SquareMatrix& operator+=(const SquareMatrix& m);
        //! Negation operator
        /*!
        \param m - const SquareMatrix&
        \return SquareMatrix&
        */
        SquareMatrix& operator-=(const SquareMatrix& m);
        //! Multiplication operator
        /*!
        \param m - const SquareMatrix&
        \return SquareMatrix&
        */
        SquareMatrix& operator*=(const SquareMatrix& m);
        //! Equal operator
        /*!
        \param m - const SquareMatrix&
        \return bool
        */
        bool operator==(const SquareMatrix& m);
        //! Print function
        /*!
        Outputs the matrix into the stream os in the form [[1,2],[3,4]]
        \param os - ostream&
        \return ostream
        */
        std::ostream& print(std::ostream &os);
        //! toString function
        /*!
        Returns string of the output in the form [[1,2],[3,4]]
        \return string
        */
        std::string toString();
    protected:
    private:
        int n; /*!< Dimensions of the square matrix. */
        std::vector<std::vector<IntElement>> elements; /*!< Elements of the square matrix inside 2D vector. */
};

#endif // SQUAREMATRIX_H
