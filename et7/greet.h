#ifndef GREET_H
#define GREET_H
#include <string>
#include <vector>

/*! \file greet.h */

//! Abstract base class
class Greeter
{
    public:
        //! Destructor
        virtual ~Greeter() {};
        //! Says greetings
        /*!
        \return greetings - const string&
         */
        virtual const std::string& greet() const = 0;
};

//! Includes saying greetings to the world in english.
class HelloGreeter : public Greeter
{
    public:
        //! Constructor.
        HelloGreeter();
        //! Destructor.
        virtual ~HelloGreeter();
        //! Says greetings in english.
        /*!
        \return greetings - const string&
         */
        const std::string& greet() const { return greetings; };
    protected:
    private:
        std::string greetings = "Hello world!"; /*!< Greetings in english. */
};

//! Includes saying greetings to the world in finnish.
class MoroGreeter : public Greeter
{
    public:
        //! Constructor.
        MoroGreeter();
        //! Destructor.
        virtual ~MoroGreeter();
        //! Says greetings in finnish.
        /*!
        \return greetings - const string&
         */
        const std::string& greet() const { return greetings; };
    protected:
    private:
        std::string greetings = "Moro maailma!"; /*!< Greetings in finnish. */
};

#endif // GREET_H
