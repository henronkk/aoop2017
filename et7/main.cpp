#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <memory>
#include "greet.h"

/*! \file main.cpp */

template<typename T>
//! Output function for greetings
/*!
    \param obj - T
*/
void print(T obj)
{
    std::cout << (*obj)->greet() << std::endl;
}

//! Function to call in case of invalid or missing argument
/*!
    \return amount of prints - int
*/
int invalidArg()
{
    std::cerr << "Invalid arguments!" << std::endl;
    std::cout << "Enter valid integer input: ";
    int val;
    std::cin >> val;
    if (val < 0)
    {
        std::cout << "Input needs to be non-negative!" << std::endl;
        val = invalidArg();
    }
    while (!std::cin)
    {
        std::cin.clear();
        std::cin.ignore(999, '\n');
        std::cout << "Enter valid integer input: ";
        std::cin >> val;
    }
    return val;
}

//! Function to print objects
/*!
    // Creates val amount of random objects inside a vector, and prints them out to the console.
    \param amount of prints - int
*/
void printObject(int val)
{
    for (int i = 0; i < val; i++)
    {
        int rnd = rand() % 2;
        if (rnd == 0)
        {
            std::unique_ptr<Greeter> hello(new HelloGreeter());
            print(&hello);
        }
        else
        {
            std::unique_ptr<Greeter> moro(new MoroGreeter());
            print(&moro);
        }
    }
}

int main(int argc, char* argv[])
{
    srand(time(0));
    int val;
    try
    {
        if (argc == 2)
        {
            int i = std::stoi(argv[1]);
            if (i < 0)
            {
                i = invalidArg();
            }
            printObject(i);
            return 0;
        }
        else
        {
            val = invalidArg();
            if (val < 0)
            {
                val = invalidArg();
            }
            printObject(val);
            return 0;
        }
    }
    catch (std::exception const &exc)
    {
        val = invalidArg();
        printObject(val);
        return 0;
    }
}
