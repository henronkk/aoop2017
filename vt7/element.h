#ifndef ELEMENT_H
#define ELEMENT_H
#include <iostream>
#include <vector>
#include <memory>
#include <map>

/** \file element.h */
using Valuation = std::map< char, int >;
extern Valuation val;
template<class T>
class TElement;
//! Includes Element base class
class Element
{
    public:
        //! Destructor
        virtual ~Element() {};
        //! Clone function
        /*!
        \return shared_ptr<Element>
        */
        virtual std::shared_ptr<Element> clone() const = 0;
        //! toString function
        /*!
        returns the string representation of Element
        \return string
        */
        virtual std::string toString() const = 0;
        //! evaluate function
        /*!
        \param val - const Valuation&
        \return int
        */
        virtual int evaluate(const Valuation& val) const = 0;
        friend std::ostream& operator<<(std::ostream& os, const Element& i);
};

//std::ostream& operator<<(std::ostream& os, const Element& i);

template<>
//! Includes IntElement class
class TElement<int> : public Element
{
    public:
        //! Default Constructor
        TElement<int>();
        //! Constructor
        /*!
        \param v - int
         */
        explicit TElement<int>(int v);
        //! Destructor.
        virtual ~TElement<int>();
        //! Value Getter
        /*!
        \return int
        */
        int getVal() const;
        //! Value Setter
        /*!
        \param v - int
        */
        void setVal(int v);
        //! Clone function
        /*!
        returns the pointer to a copy of this
        \return TElement<int>*
        */
        std::shared_ptr<Element> clone() const;
        //! toString function
        /*!
        returns the string representation of val
        \return string
        */
        std::string toString() const;
        //! evaluate function
        /*!
        returns val
        \param val - const Valuation&
        \return int
        */
        int evaluate(const Valuation& val) const;
        //! Add AND assignment operator
        /*!
        \param i - TElement<int>
        \return TElement<int>&
        */
        TElement<int>& operator+=(const TElement<int>& i);
        //! Subtract AND assignment operator
        /*!
        \param i - const TElement<int>&
        \return TElement<int>&
        */
        TElement<int>& operator-=(const TElement<int>& i);
        //! Multiply AND assignment operator
        /*!
        \param i - const TElement<int>&
        \return TElement<int>&
        */
        TElement<int>& operator*=(const TElement<int>& i);
        //! Add operator
        /*!
        \param i - const TElement<int>&
        \return TElement<int>
        */
        TElement<int> operator+(const TElement<int>& i);
        //! Subtract operator
        /*!
        \param i - const TElement<int>&
        \return TElement<int>
        */
        TElement<int> operator-(const TElement<int>& i);
        //! Multiply operator
        /*!
        \param i - const TElement<int>&
        \return TElement<int>
        */
        TElement<int> operator*(const TElement<int>& i);
        //! Equal operator
        /*!
        \param m - const TElement<int>&
        \return bool
        */
        bool operator==(const TElement<int>& i) const;
        //! Print operator
        /*!
        \param os - ostream&
        \param i - const TElement<int>&
        \return ostream&
        */
        friend std::ostream& operator<<(std::ostream& os, const TElement<int>& i);
    protected:
    private:
        int val; /*!< Value of matrix element */
};

template<>
//! Includes VariableElement class
class TElement<char> : public Element
{
    public:
        //! Default Constructor
        TElement<char>();
        //! Constructor
        /*!
        \param v - char
         */
        explicit TElement<char>(char v);
        //! Destructor.
        virtual ~TElement<char>();
        //! Value Getter
        /*!
        \return const char
        */
        char getVal() const;
        //! Value Setter
        /*!
        \param v - char
        */
        void setVal(char v);
        //! Clone function
        /*!
        returns the pointer to a copy of this
        \return TElement<char>*
        */
        std::shared_ptr<Element> clone() const;
        //! toString function
        /*!
        returns the string representation of var
        \return string
        */
        std::string toString() const;
        //! evaluate function
        /*!
        returns val[var] or throws an exception
        \param val - const Valuation&
        \return int
        */
        int evaluate(const Valuation& val) const;
        //! Equal operator
        /*!
        \param m - const TElement<char>&
        \return bool
        */
        bool operator==(const TElement<char>& i) const;
        //! Print operator
        /*!
        \param os - ostream&
        \param i - const TElement<char>&
        \return ostream&
        */
        friend std::ostream& operator<<(std::ostream& os, const TElement<char>& i);
    protected:
    private:
        char var; /*!< Variable of matrix element */
};

#endif // ELEMENT_H
