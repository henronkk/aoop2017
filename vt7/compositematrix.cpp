#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <cstring>
#include <functional>
#include "elementarymatrix.h"
#include "compositematrix.h"

/** \file compositematrix.cpp */

//Valuation val = {{'a',1},{'b',2},{'c',3},{'d',4},{'x',2},{'y',3}};
//Valuation key_val_pairs = {{'a',1},{'b',2},{'c',3},{'x',2},{'y',3}};
//Valuation fake_keys = {{'f',1},{'g',2}};

CompositeSquareMatrix::CompositeSquareMatrix()
{

}

CompositeSquareMatrix::CompositeSquareMatrix(const SquareMatrix& op1, const SquareMatrix& op2,
                                const std::function<ConcreteSquareMatrix(const ConcreteSquareMatrix&,
                                const ConcreteSquareMatrix&)>& opr, char opc)
{
    oprnd1 = op1.clone();
    oprnd2 = op2.clone();
    oprtor = opr;
    oprtor(op1.evaluate(val),op2.evaluate(val));
    op_char = opc;
}

CompositeSquareMatrix::CompositeSquareMatrix(const CompositeSquareMatrix& m)
{
    oprnd1 = m.oprnd1;
    oprnd2 = m.oprnd2;
    oprtor = m.oprtor;
    op_char = m.op_char;
}

CompositeSquareMatrix::CompositeSquareMatrix(CompositeSquareMatrix&& m)
{
    oprnd1 = m.oprnd1;
    oprnd2 = m.oprnd2;
    oprtor = m.oprtor;
    op_char = m.op_char;
}
CompositeSquareMatrix::~CompositeSquareMatrix()
{
    //destructor
}
CompositeSquareMatrix& CompositeSquareMatrix::operator=(const CompositeSquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    oprnd1 = m.oprnd1;
    oprnd2 = m.oprnd2;
    oprtor = m.oprtor;
    op_char = m.op_char;
    return *this;
}

CompositeSquareMatrix& CompositeSquareMatrix::operator=(CompositeSquareMatrix&& m)
{
    if (this == &m)
        {
            return *this;
        }
    oprnd1 = std::move(m.oprnd1);
    oprnd2 = std::move(m.oprnd2);
    oprtor = std::move(m.oprtor);
    op_char = std::move(m.op_char);
    return *this;
}

std::shared_ptr<SquareMatrix> CompositeSquareMatrix::clone() const
{
    std::shared_ptr<SquareMatrix> cloned(new CompositeSquareMatrix(*this));
    return cloned;
}

void CompositeSquareMatrix::print(std::ostream& os) const
{
    os << toString();
}

std::string CompositeSquareMatrix::toString() const
{
    return oprtor(oprnd1->evaluate(val),oprnd2->evaluate(val)).toString();
}

ConcreteSquareMatrix CompositeSquareMatrix::evaluate(const Valuation& val) const
{
    return oprtor(oprnd1->evaluate(val),oprnd2->evaluate(val));
}
