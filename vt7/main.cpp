#include <memory>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <iostream>
#include <stack>
#include "compositematrix.h"
#include "elementarymatrix.h"
#include "element.h"
#include "squarematrix.h"

/*! \mainpage VT6 documentation */
/** \file main.cpp */

Valuation val;
using IntElement = TElement<int>;
using VariableElement = TElement<char>;
using ConcreteSquareMatrix = ElementarySquareMatrix<IntElement>;
using SymbolicSquareMatrix = ElementarySquareMatrix<Element>;

//! Addition function
/*!
        \param op1 - const ConcreteSquareMatrix&
        \param op2 - const ConcreteSquareMatrix&
        \return ConcreteSquareMatrix
*/
ConcreteSquareMatrix mat_add(const ConcreteSquareMatrix& op1, const ConcreteSquareMatrix& op2)
{
    ConcreteSquareMatrix add1 = op1;
    ConcreteSquareMatrix add2 = op2;
    ConcreteSquareMatrix add_result = (add1+=add2);
    return add_result;
}

//! Negation function
/*!
        \param op1 - const ConcreteSquareMatrix&
        \param op2 - const ConcreteSquareMatrix&
        \return ConcreteSquareMatrix
*/
ConcreteSquareMatrix mat_neg(const ConcreteSquareMatrix& op1, const ConcreteSquareMatrix& op2)
{
    ConcreteSquareMatrix neg1 = op1;
    ConcreteSquareMatrix neg2 = op2;
    ConcreteSquareMatrix neg_result = (neg1-=neg2);
    return neg_result;
}

//! Multiplication function
/*!
        \param op1 - const ConcreteSquareMatrix&
        \param op2 - const ConcreteSquareMatrix&
        \return ConcreteSquareMatrix
*/
ConcreteSquareMatrix mat_mul(const ConcreteSquareMatrix& op1, const ConcreteSquareMatrix& op2)
{
    ConcreteSquareMatrix mul1 = op1;
    ConcreteSquareMatrix mul2 = op2;
    ConcreteSquareMatrix mul_result = (mul1*=mul2);
    return mul_result;
}

int main()
{
    std::stack<SymbolicSquareMatrix> mat_que;
    std::string input_str;
    std::string output_str;
    std::cout << "Matrix calculation tool" << std::endl;
    std::cout << "-------------------------------------------------------------------" << std::endl;
    std::cout << "[[1,x][y,4]]     -     n*n matrix input format" << std::endl;
    std::cout << "x=5              -     valuation format (valuate before operations)" << std::endl;
    std::cout << "'+' '-' '*'      -     operations" << std::endl;
    std::cout << "'='              -     evaluate the equation" << std::endl;
    std::cout << "'quit'           -     exit the program" << std::endl;
    std::cout << "-------------------------------------------------------------------" << std::endl;
    while(1)
    {
        std::cin >> input_str;
        if (input_str=="+")
        {
            SymbolicSquareMatrix sqmat2(mat_que.top());
            mat_que.pop();
            SymbolicSquareMatrix sqmat1(mat_que.top());
            mat_que.pop();
            std::stringstream ss;
            if (output_str.size() > 0)
            {
                ss << "(" << output_str << ") + (" << sqmat2.toString() << ")";
            }
            else
            {
                ss << "(" << sqmat1.toString() << ") + (" << sqmat2.toString() << ")";
            }
            output_str = ss.str();
            CompositeSquareMatrix sqmat3(sqmat1,sqmat2,mat_add,'+');
            SymbolicSquareMatrix sqmat4(sqmat3.toString());
            mat_que.push(sqmat4);
            std::cout << "          " << output_str << std::endl;
        }
        if (input_str=="-")
        {
            SymbolicSquareMatrix sqmat2(mat_que.top());
            mat_que.pop();
            SymbolicSquareMatrix sqmat1(mat_que.top());
            mat_que.pop();
            std::stringstream ss;
            if (output_str.size() > 0)
            {
                ss << "(" << output_str << ") - (" << sqmat2.toString() << ")";
            }
            else
            {
                ss << "(" << sqmat1.toString() << ") - (" << sqmat2.toString() << ")";
            }
            output_str = ss.str();
            CompositeSquareMatrix sqmat3(sqmat1,sqmat2,mat_neg,'-');
            SymbolicSquareMatrix sqmat4(sqmat3.toString());
            mat_que.push(sqmat4);
            std::cout << "          " << output_str << std::endl;
        }
        if (input_str=="*")
        {
            SymbolicSquareMatrix sqmat2(mat_que.top());
            mat_que.pop();
            SymbolicSquareMatrix sqmat1(mat_que.top());
            mat_que.pop();
            std::stringstream ss;
            if (output_str.size() > 0)
            {
                ss << "(" << output_str << ") * (" << sqmat2.toString() << ")";
            }
            else
            {
                ss << "(" << sqmat1.toString() << ") * (" << sqmat2.toString() << ")";
            }
            output_str = ss.str();
            CompositeSquareMatrix sqmat3(sqmat1,sqmat2,mat_mul,'*');
            SymbolicSquareMatrix sqmat4(sqmat3.toString());
            mat_que.push(sqmat4);
            std::cout << "          " << output_str << std::endl;
        }
        if (input_str=="=")
        {
            std::cout << "          " << mat_que.top().toString() << std::endl;
        }
        if (isalpha(input_str[0]) && input_str[1]=='=')
        {
            std::string num_val;
            for(int i = 2;i < input_str.size();i++)
            {
               num_val.push_back(input_str[i]);
            }
            std::map<char,int>::iterator it;
            it = val.find(input_str[0]);
            if (it != val.end())
            {
                val.erase(it);
            }
            val.insert(std::pair<char,int>(input_str[0],std::stoi(num_val)));
            num_val.clear();
        }
        if (input_str=="quit")
        {
            return 0;
        }
        if(input_str[0]=='[' && input_str[1]=='[')
        {
            SymbolicSquareMatrix sqmat(input_str);
            mat_que.push(sqmat);
        }
    }
}
