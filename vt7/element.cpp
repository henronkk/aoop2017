#include <iostream>
#include <vector>
#include <string>
#include "element.h"

/** \file element.cpp */

std::shared_ptr<Element> Element::clone() const
{
   // clone element to pointer
}

std::string Element::toString() const
{
    // return element as string
}

int Element::evaluate(const Valuation& val) const
{
    // evaluate var to val
}

std::ostream& operator<<(std::ostream& os, const Element& i)
{
    os << i;
    return os;
}

//------------------------------------------------
TElement<int>::TElement()
{

}
TElement<int>::TElement(int v)
{
    setVal(v);
}

TElement<int>::~TElement()
{
    //std::cout << "TElement<int> destructor called" << std::endl;
}

int TElement<int>::getVal() const {
    return val;
}

void TElement<int>::setVal(int v) {
    val = v;
}

std::shared_ptr<Element> TElement<int>::clone() const
{
    std::shared_ptr<Element> cloned(new TElement<int>(this->getVal()));
    return cloned;
}

std::string TElement<int>::toString() const
{
    std::string str = std::to_string(val);
    return str;
}

int TElement<int>::evaluate(const Valuation& val) const
{
    return this->val;
}

TElement<int>& TElement<int>::operator+=(const TElement<int>& i)
{
    if (this == &i)
        {
            return *this;
        }
    val += i.val;
    return *this;
}

TElement<int>& TElement<int>::operator-=(const TElement<int>& i)
{
    if (this == &i)
        {
            return *this;
        }
    val -= i.val;
    return *this;
}

TElement<int>& TElement<int>::operator*=(const TElement<int>& i)
{
    if (this == &i)
        {
            return *this;
        }
    val *= i.val;
    return *this;
}

TElement<int> TElement<int>::operator+(const TElement<int>& i)
{
    if (this == &i)
        {
            return *this;
        }
    return TElement<int>(val + i.val);
}

TElement<int> TElement<int>::operator-(const TElement<int>& i)
{
    if (this == &i)
        {
            return *this;
        }
    return TElement<int>(val - i.val);
}

TElement<int> TElement<int>::operator*(const TElement<int>& i)
{
    if (this == &i)
        {
            return *this;
        }
    return TElement<int>(val * i.val);
}

bool TElement<int>::operator==(const TElement<int>& i) const
{
    return val == i.val;
}

std::ostream& operator<<(std::ostream& os, const TElement<int>& i)
{
    os << i.val;
    return os;
}
//------------------------------------------------
TElement<char>::TElement()
{

}

TElement<char>::TElement(char v)
{
    setVal(v);
}

TElement<char>::~TElement()
{
    //std::cout << "TElement<char> destructor called" << std::endl;
}

char TElement<char>::getVal() const
{
    return var;
}

void TElement<char>::setVal(char v) {
    var = v;
}

std::shared_ptr<Element> TElement<char>::clone() const
{
    std::shared_ptr<Element> cloned(new TElement<char>(this->getVal()));
    return cloned;
}

std::string TElement<char>::toString() const
{
    std::string str;
    str.push_back(var);
    return str;
}

int TElement<char>::evaluate(const Valuation& val) const
{
    try
    {
        return val.at(this->var);
    }
    catch (std::out_of_range& err)
    {
        throw std::out_of_range("That key does not have value!");
    }
}

bool TElement<char>::operator==(const TElement<char>& i) const
{
    return var == i.var;
}

std::ostream& operator<<(std::ostream& os, const TElement<char>& i)
{
    os << i.var;
    return os;
}
