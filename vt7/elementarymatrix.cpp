#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <cstring>
#include <math.h>
#include "elementarymatrix.h"

/** \file elementarymatrix.cpp */
ElementarySquareMatrix<IntElement>::ElementarySquareMatrix()
{

}

ElementarySquareMatrix<IntElement>::ElementarySquareMatrix(const std::string& str_m)
{
    std::stringstream ss{str_m};
    std::vector<int> values;
    char c;
    int v;
    int p = 0;
    int num;
    ss >> c;
    ss >> c;
    while(c != ']')
    {
        while(c != ']')
        {
            ss >> v;
            values.push_back(v);
            ss >> c;
        }
        ss >> c;
    }
    n = sqrt(values.size());
    for (int i = 0; i < n; i++)
    {
        std::vector<std::shared_ptr<IntElement>> col;
        for (int j = 0; j < n; j++)
        {
            num = values[p];
            std::shared_ptr<IntElement> numbs(new IntElement(num));
            col.push_back(std::move(numbs));
            p++;
        }
        elements.push_back(std::move(col));
    }
}

ElementarySquareMatrix<IntElement>::ElementarySquareMatrix(int m)
{
    int ran_mat[m][m];
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < m; j++)
        {
            ran_mat[i][j] = rand() % 198 - 99; // Values between -99 and 99
        }
    }
    std::stringstream os;
    os << "[";
    for (int i = 0; i < m; i++)
    {
        os << "[";
        for (int j = 0; j < m; j++)
        {
            os << ran_mat[i][j];
            if (j < m-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    ElementarySquareMatrix<IntElement> rnd(os.str());
    n = rnd.n;
    elements = rnd.elements;
}

ElementarySquareMatrix<IntElement>::ElementarySquareMatrix(const ElementarySquareMatrix<IntElement>& m)
{
    n = m.n;
    elements = m.elements;
}

ElementarySquareMatrix<IntElement>::ElementarySquareMatrix(ElementarySquareMatrix<IntElement>&& m)
{
    n = m.n;
    elements = m.elements;
}

ElementarySquareMatrix<IntElement>::~ElementarySquareMatrix()
{
    //std::cout << "Destructor called" << std::endl;
}

ElementarySquareMatrix<IntElement>& ElementarySquareMatrix<IntElement>::operator=(const ElementarySquareMatrix<IntElement>& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = m.n;
    elements = m.elements;
    return *this;
}

ElementarySquareMatrix<IntElement>& ElementarySquareMatrix<IntElement>::operator=(ElementarySquareMatrix<IntElement>&& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = std::move(m.n);
    elements = std::move(m.elements);
    return *this;
}

std::shared_ptr<SquareMatrix> ElementarySquareMatrix<IntElement>::clone() const
{
    std::shared_ptr<SquareMatrix> cloned(new ElementarySquareMatrix<IntElement>(*this));
    return cloned;
}

ElementarySquareMatrix<IntElement> ElementarySquareMatrix<IntElement>::transpose() const
{
    int tmp[n][n];
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            int x = elements[i][j]->getVal();
            tmp[i][j] = x;
        }
    }
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[j][i]);
        }
    }
    return *this;
}

ElementarySquareMatrix<IntElement>& ElementarySquareMatrix<IntElement>::operator+=(const ElementarySquareMatrix<IntElement>& m)
{
    if (this == &m)
        {
            return *this;
        }
    int tmp[n][n];
    memset(tmp, 0, sizeof(tmp[0][0])*n*n);
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            int x = (elements[i][j]->getVal() + m.elements[i][j]->getVal());
            tmp[i][j] = x;
        }
    }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[i][j]);
        }
    }
    return *this;
}

ElementarySquareMatrix<IntElement>& ElementarySquareMatrix<IntElement>::operator-=(const ElementarySquareMatrix<IntElement>& m)
{
    if (this == &m)
        {
            return *this;
        }
    int tmp[n][n];
    memset(tmp, 0, sizeof(tmp[0][0])*n*n);
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            int x = (elements[i][j]->getVal() - m.elements[i][j]->getVal());
            tmp[i][j] = x;
        }
    }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[i][j]);
        }
    }
    return *this;
}

ElementarySquareMatrix<IntElement>& ElementarySquareMatrix<IntElement>::operator*=(const ElementarySquareMatrix<IntElement>& m)
{
    if (this == &m)
        {
            return *this;
        }
    int tmp[n][n];
    memset(tmp, 0, sizeof(tmp[0][0])*n*n);
    for (int i = 0;i < n;i++)
    {
        for (int j = 0;j < n;j++)
        {
            for(int k = 0;k < n;k++)
            {
                int x = (elements[i][k]->getVal() * m.elements[k][j]->getVal());
                tmp[i][j] += x;
            }
        }
    }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[i][j]);
        }
    }
    return *this;
}

bool ElementarySquareMatrix<IntElement>::operator==(const ElementarySquareMatrix<IntElement>& m) const
{
    return elements.size() == m.elements.size();
}

void ElementarySquareMatrix<IntElement>::print(std::ostream& os) const
{
    os << toString();
}

std::string ElementarySquareMatrix<IntElement>::toString() const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[i][j]->getVal();
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    return os.str();
}

ElementarySquareMatrix<IntElement> ElementarySquareMatrix<IntElement>::evaluate(const Valuation& val) const
{
    ElementarySquareMatrix<IntElement> copied(*this);
    return copied;
}

std::ostream& operator<<(std::ostream& os, const ElementarySquareMatrix<IntElement>& m)
{
    os << m.toString();
    return os;
}

ElementarySquareMatrix<Element>::ElementarySquareMatrix()
{

}

ElementarySquareMatrix<Element>::ElementarySquareMatrix(const std::string& str_m)
{
    std::stringstream ss{str_m};
    std::vector<int> values;
    std::vector<int> checker;
    char c;
    int v;
    int p = 0;
    ss >> c;
    ss >> c;
    while(c != ']')
    {
        while(c != ']')
        {
            if (isdigit(ss.peek()))
            {
                ss >> v;
                values.push_back(v);
                checker.push_back(1);
            }
            if (isalpha(ss.peek()))
            {
                ss >> c;
                values.push_back(c);
                checker.push_back(0);
            }
            ss >> c;
        }
        ss >> c;
    }
    n = sqrt(values.size());
    for (int i = 0; i < n; i++)
    {
        std::vector<std::shared_ptr<Element>> col;
        for (int j = 0; j < n; j++)
        {
            if (checker[p] == 1)
            {
                std::shared_ptr<IntElement> numb(new IntElement(values[p]));
                col.push_back(std::move(numb));
                p++;
            }
            else
            {
                std::shared_ptr<VariableElement> varb(new VariableElement(char(values[p])));
                col.push_back(std::move(varb));
                p++;
            }
        }
        elements.push_back(std::move(col));
    }
}

ElementarySquareMatrix<Element>::ElementarySquareMatrix(const ElementarySquareMatrix<Element>& m)
{
    n = m.n;
    elements = m.elements;
}

ElementarySquareMatrix<Element>::ElementarySquareMatrix(ElementarySquareMatrix<Element>&& m)
{
    n = m.n;
    elements = m.elements;
}

ElementarySquareMatrix<Element>::~ElementarySquareMatrix<Element>()
{
    //std::cout << "Destructor called" << std::endl;
}

ElementarySquareMatrix<Element>& ElementarySquareMatrix<Element>::operator=(const ElementarySquareMatrix<Element>& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = m.n;
    elements = m.elements;
    return *this;
}

ElementarySquareMatrix<Element>& ElementarySquareMatrix<Element>::operator=(ElementarySquareMatrix<Element>&& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = std::move(m.n);
    elements = std::move(m.elements);
    return *this;
}

std::shared_ptr<SquareMatrix> ElementarySquareMatrix<Element>::clone() const
{
    std::shared_ptr<SquareMatrix> cloned(new ElementarySquareMatrix<Element>(*this));
    return cloned;
}

ElementarySquareMatrix<Element> ElementarySquareMatrix<Element>::transpose() const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[j][i]->toString();
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    ElementarySquareMatrix<Element> tmp(os.str());
    return tmp;
}

bool ElementarySquareMatrix<Element>::operator==(const ElementarySquareMatrix<Element>& m) const
{
    return elements.size() == m.elements.size();
}

void ElementarySquareMatrix<Element>::print(std::ostream& os) const
{
    os << toString();
}

std::string ElementarySquareMatrix<Element>::toString() const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[i][j]->toString();
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    return os.str();
}

ConcreteSquareMatrix ElementarySquareMatrix<Element>::evaluate(const Valuation& val) const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[i][j]->evaluate(val);
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    ConcreteSquareMatrix tmp(os.str());
    return tmp;
}

std::ostream& operator<<(std::ostream& os, const ElementarySquareMatrix<Element>& m)
{
    os << m.toString();
    return os;
}
