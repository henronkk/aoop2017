#include "squarematrix.h"

/** \file squarematrix.cpp */

std::ostream& operator<<(std::ostream& os, const SquareMatrix& m)
{
    os << m.toString();
    return os;
}
