#ifndef ELEMENTARYMATRIX_H
#define ELEMENTARYMATRIX_H
#include "squarematrix.h"
#include "element.h"

/** \file elementarymatrix.h */

using IntElement = TElement<int>;
using VariableElement = TElement<char>;

template<typename T>
class ElementarySquareMatrix;
template<>
//! Class to create a ConcreteSquareMatrix
class ElementarySquareMatrix<IntElement> : public SquareMatrix
{
    public:
        //! Default Constructor
        ElementarySquareMatrix<IntElement>();
        //! Constructor
        /*!
        Constructs a matrix from the string representation of the form [[1,2][3,4]]
        \param str_m - const std::string&
        */
        explicit ElementarySquareMatrix<IntElement>(const std::string& str_m);
        //! Random constructor
        /*!
        constructs random m*m square matrix
        \param m - int
        */
        explicit ElementarySquareMatrix<IntElement>(int m);
        //! Copy constructor
        /*!
        \param m - const ElementarySquareMatrix<IntElement>&
        */
        ElementarySquareMatrix<IntElement>(const ElementarySquareMatrix<IntElement>& m);
        //! Transfer copy constructor
        /*!
        \param m - ElementarySquareMatrix<IntElement>&&
        */
        ElementarySquareMatrix<IntElement>(ElementarySquareMatrix<IntElement>&& m);
        //! Destructor
        virtual ~ElementarySquareMatrix<IntElement>();
        //! Assignment operator
        /*!
        \param m - const ElementarySquareMatrix<IntElement>&
        \return ElementarySquareMatrix<IntElement>&
        */
        ElementarySquareMatrix<IntElement>& operator=(const ElementarySquareMatrix<IntElement>& m);
        //! Move assignment operator
        /*!
        \param m - ElementarySquareMatrix<IntElement>&&
        \return ElementarySquareMatrix<IntElement>&
        */
        ElementarySquareMatrix<IntElement>& operator=(ElementarySquareMatrix<IntElement>&& m);
        //! Clone function
        /*!
        \return const shared_ptr<SquareMatrix>
        */
        std::shared_ptr<SquareMatrix> clone() const;
        //! Transpose matrix
        /*!
        \return ElementarySquareMatrix<IntElement>
        */
        ElementarySquareMatrix<IntElement> transpose() const;
        //! Addition operator
        /*!
        \param m - const ElementarySquareMatrix<IntElement>&
        \return ElementarySquareMatrix<IntElement>&
        */
        ElementarySquareMatrix<IntElement>& operator+=(const ElementarySquareMatrix<IntElement>& m);
        //! Negation operator
        /*!
        \param m - const ElementarySquareMatrix<IntElement>&
        \return ElementarySquareMatrix<IntElement>&
        */
        ElementarySquareMatrix<IntElement>& operator-=(const ElementarySquareMatrix<IntElement>& m);
        //! Multiplication operator
        /*!
        \param m - const ElementarySquareMatrix<IntElement>&
        \return ElementarySquareMatrix<IntElement>&
        */
        ElementarySquareMatrix<IntElement>& operator*=(const ElementarySquareMatrix<IntElement>& m);
        //! Equal operator
        /*!
        \param m - const ElementarySquareMatrix<IntElement>&
        \return bool
        */
        bool operator==(const ElementarySquareMatrix<IntElement>& m) const;
        //! Print function
        /*!
        Outputs the matrix into the stream os in the form [[1,2],[3,4]]
        \param os - ostream&
        \return ostream
        */
        void print(std::ostream &os) const;
        //! toString function
        /*!
        Returns string of the output in the form [[1,2],[3,4]]
        \return string
        */
        std::string toString() const;
        //! Evaluate function
        /*!
        returns copy of this
        \param val - const Valuation&
        \return const ElementarySquareMatrix<IntElement>
        */
        ElementarySquareMatrix<IntElement> evaluate(const Valuation& val) const override;
        friend std::ostream& operator<<(std::ostream& os, const SquareMatrix& m);
    protected:
    private:
        unsigned int n; /*!< Dimensions of the square matrix. */
        std::vector<std::vector<std::shared_ptr<IntElement>>> elements; /*!< Elements of the square matrix inside 2D vector. */
};

template<>
//! Class to create a SymbolicSquareMatrix
class ElementarySquareMatrix<Element> : public SquareMatrix
{
    public:
        //! Default Constructor
        ElementarySquareMatrix<Element>();
        //! Constructor
        /*!
        Constructs a matrix from the string representation of the form [[1,x][y,4]]
        \param str_m - const std::string&
        */
        explicit ElementarySquareMatrix<Element>(const std::string& str_m);
        //! Copy constructor
        /*!
        \param m - const ElementarySquareMatrix<Element>&
        */
        ElementarySquareMatrix<Element>(const ElementarySquareMatrix<Element>& m);
        //! Transfer copy constructor
        /*!
        \param m - ElementarySquareMatrix<Element>&&
        */
        ElementarySquareMatrix<Element>(ElementarySquareMatrix<Element>&& m);
        //! Destructor
        virtual ~ElementarySquareMatrix<Element>();
        //! Assignment operator
        /*!
        \param m - const ElementarySquareMatrix<Element>&
        \return ElementarySquareMatrix<Element>&
        */
        ElementarySquareMatrix<Element>& operator=(const ElementarySquareMatrix<Element>& m);
        //! Move assignment operator
        /*!
        \param m - ElementarySquareMatrix<Element>&&
        \return ElementarySquareMatrix<Element>&
        */
        ElementarySquareMatrix<Element>& operator=(ElementarySquareMatrix<Element>&& m);
        //! Clone function
        /*!
        \return shared_ptr<SquareMatrix>
        */
        std::shared_ptr<SquareMatrix> clone() const;
        //! Transpose matrix
        /*!
        \return ElementarySquareMatrix<Element>
        */
        ElementarySquareMatrix<Element> transpose() const;
        //! Equal operator
        /*!
        \param m - const ElementarySquareMatrix<Element>&
        \return bool
        */
        bool operator==(const ElementarySquareMatrix<Element>& m) const;
        //! Print function
        /*!
        Outputs the matrix into the stream os in the form [[1,x],[y,4]]
        \param os - ostream&
        \return ostream
        */
        void print(std::ostream &os) const;
        //! toString function
        /*!
        Returns string of the output in the form [[1,x],[y,4]]
        \return string
        */
        std::string toString() const;
        //! evaluate function
        /*!
        returns an instance of ConcreteSquareMatrix obtained by applying val to each element or throws an exception
        \param val - const Valuation
        \return ConcreteSquareMatrix
        */
        ElementarySquareMatrix<IntElement> evaluate(const Valuation& val) const override;
        //! output operator
        /*!
        \param os - std::ostream&
        \param m - const SquareMatrix&
        \return std::ostream&
        */
        friend std::ostream& operator<<(std::ostream& os, const SquareMatrix& m);
    protected:
    private:
        unsigned int n; /*!< Dimensions of the square matrix. */
        std::vector<std::vector<std::shared_ptr<Element>>> elements; /*!< Elements of the square matrix inside 2D vector. */
};

#endif // ELEMENTARYMATRIX_H
