#ifndef SQUAREMATRIX_H
#define SQUAREMATRIX_H
#include <iostream>
#include "intelement.h"

/** \file squarematrix.h */
//! Class to create a SquareMatrix
class SquareMatrix
{
    public:
        //! Default Constructor.
        SquareMatrix();
        //! Constructor
        /*!
        \param i11 - const IntElement&
        \param i12 - const IntElement&
        \param i21 - const IntElement&
        \param i22 - const IntElement&
        */
        SquareMatrix(const IntElement& i11,const IntElement& i12,const IntElement& i21,const IntElement& i22);
        //! Copy constructor
        /*!
        \param m - const SquareMatrix&
        */
        SquareMatrix(const SquareMatrix& m);
        //! Destructor.
        virtual ~SquareMatrix();
        //! Addition operator
        /*!
        \param m - const SquareMatrix&
        \return SquareMatrix&
        */
        SquareMatrix& operator+=(const SquareMatrix& m);
        //! Negation operator
        /*!
        \param m - const SquareMatrix&
        \return SquareMatrix&
        */
        SquareMatrix& operator-=(const SquareMatrix& m);
        //! Multiplication operator
        /*!
        \param m - const SquareMatrix&
        \return SquareMatrix&
        */
        SquareMatrix& operator*=(const SquareMatrix& m);
        //! Print function
        /*!
        Outputs the matrix into the stream os in the form [[1,2],[3,4]]
        \param os - ostream&
        \return ostream
        */
        std::ostream& print(std::ostream &os);
        //! toString function
        /*!
        Returns string of the output in the form [[1,2],[3,4]]
        \return string
        */
        std::string toString();
    protected:
    private:
        IntElement e11; /*!< Value of matrix element 11*/
        IntElement e12; /*!< Value of matrix element 12*/
        IntElement e21; /*!< Value of matrix element 21*/
        IntElement e22; /*!< Value of matrix element 22*/
};

#endif // SQUAREMATRIX_H
