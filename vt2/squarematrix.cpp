#define CATCH_CONFIG_MAIN
#include <iostream>
#include <string>
#include <sstream>
#include "catch.hpp"
#include "squarematrix.h"

TEST_CASE( "IntElement initializing & operator testing", "[element]" )
{
    std::cout << "IntElement initializing & operator testing" << std::endl;
    std::cout << ".........................................." << std::endl;
    IntElement element1(1), element2(2), element3(3), element4(4);
    IntElement element5(5), element6(6), element7(7), element8(8);
    std::cout << "Testing getter... " << std:: endl;
    REQUIRE( element1.getVal() == 1 );
    REQUIRE( element5.getVal() == 5 );
    std::cout << "Testing print-operator..." << std::endl;;
    REQUIRE( std::cout << "Testing element addition...       " << element1 << "+" << element2 << "=" << element1+element2 << std::endl );
    REQUIRE( std::cout << "Testing element subtraction...    " << element3 << "-" << element4 << "=" << element3-element4 << std::endl );
    REQUIRE( std::cout << "Testing element multiplication... " << element5 << "*" << element6 << "=" << element5*element6 << std::endl );
}
TEST_CASE( "SquareMatrix initializing & operator testing", "[sqmat]" )
{
    std::cout << "\nSquareMatrix initializing & operator testing" << std::endl;
    std::cout << "............................................" << std::endl;
    IntElement element1(1), element2(2), element3(3), element4(4);
    IntElement element5(5), element6(6), element7(7), element8(8);
    SquareMatrix sqmat1(element1, element2, element3, element4);
    SquareMatrix sqmat2(element5, element6, element7, element8);
    SquareMatrix sqmat3 = sqmat1;
    SquareMatrix sqmat4 = sqmat1;
    SquareMatrix sqmat5 = sqmat1;
    sqmat3+=sqmat2; // Addition
    sqmat4-=sqmat2; // Subtraction
    sqmat5*=sqmat2; // Multiplication
    std::cout << "Testing matrix initialization... ";
    sqmat1.print(std::cout);
    std::cout << "Testing matrix addition...       ";
    sqmat3.print(std::cout);
    std::cout << "Testing matrix subtraction...    ";
    sqmat4.print(std::cout);
    std::cout << "Testing matrix multiplication... ";
    sqmat5.print(std::cout);
    REQUIRE( sqmat1.toString() == "[[1,2],[3,4]]" );
    REQUIRE( sqmat2.toString() == "[[5,6],[7,8]]" );
    REQUIRE( sqmat3.toString() == "[[6,8],[10,12]]" );
    REQUIRE( sqmat4.toString() == "[[-4,-4],[-4,-4]]" );
    REQUIRE( sqmat5.toString() == "[[19,22],[43,50]]" );
}
SquareMatrix::SquareMatrix()
{
}

SquareMatrix::SquareMatrix(const IntElement& i11, const IntElement& i12, const IntElement& i21, const IntElement& i22)
{
    e11 = i11;
    e12 = i12;
    e21 = i21;
    e22 = i22;
}
SquareMatrix::SquareMatrix(const SquareMatrix& m) {
    e11 = m.e11;
    e12 = m.e12;
    e21 = m.e21;
    e22 = m.e22;
}

SquareMatrix::~SquareMatrix()
{
}

SquareMatrix& SquareMatrix::operator+=(const SquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    e11 += m.e11;
    e12 += m.e12;
    e21 += m.e21;
    e22 += m.e22;
    return *this;
}

SquareMatrix& SquareMatrix::operator-=(const SquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    e11 -= m.e11;
    e12 -= m.e12;
    e21 -= m.e21;
    e22 -= m.e22;
    return *this;
}

SquareMatrix& SquareMatrix::operator*=(const SquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    SquareMatrix tmp = *this;
    tmp.e11 = ((e11 * m.e11) + (e12 * m.e21));
    tmp.e12 = ((e11 * m.e12) + (e12 * m.e22));
    tmp.e21 = ((e21 * m.e11) + (e22 * m.e21));
    tmp.e22 = ((e21 * m.e12) + (e22 * m.e22));
    e11 = tmp.e11;
    e12 = tmp.e12;
    e21 = tmp.e21;
    e22 = tmp.e22;
    return *this;
}

std::ostream& SquareMatrix::print(std::ostream& os)
{
    os << "[[" << e11.getVal() << "," << e12.getVal() << "],[" << e21.getVal() << "," << e22.getVal() << "]]" << std::endl;
    return os;
}

std::string SquareMatrix::toString()
{
    std::string result;
    //std::cout << "[[" << e11.getVal() << "],[" << e12.getVal() << "],[" << e21.getVal() << "],[" << e22.getVal() << "]]" << std::endl;
    result = "[[" + std::to_string(e11.getVal()) + "," + std::to_string(e12.getVal()) + "],[" + std::to_string(e21.getVal()) + "," + std::to_string(e22.getVal()) + "]]";
    return result;
}
