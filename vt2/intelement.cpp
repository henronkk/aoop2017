#include <iostream>
#include "intelement.h"

IntElement::IntElement()
{

}

IntElement::IntElement(int v)
{
    setVal(v);
}

IntElement::~IntElement()
{

}

int IntElement::getVal() {
    return val;
}

void IntElement::setVal(int v) {
    val = v;
}

IntElement& IntElement::operator+=(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    val += i.val;
    return *this;
}

IntElement& IntElement::operator-=(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    val -= i.val;
    return *this;
}

IntElement& IntElement::operator*=(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    val *= i.val;
    return *this;
}

IntElement IntElement::operator+(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    return IntElement(val + i.val);
}

IntElement IntElement::operator-(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    return IntElement(val - i.val);
}

IntElement IntElement::operator*(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    return IntElement(val * i.val);
}

std::ostream& operator<<(std::ostream& os, const IntElement& i)
{
    os << i.val;
    return os;
}
