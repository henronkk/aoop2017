#include <string>
#include <iostream>
#include <cstdlib>
#include "matrix.h"

using namespace std;

//!  Main
/*!
  Amount of rows and columns is given either by argument or manually by the user.
  If inputs are invalid, user is asked to give the correct inputs.
  Matrix is then printed out in a form of ij where i is the row number and j is the column number.
*/

int main(int argc, char* argv[])
{
    int i;
    int j;
    if (argc == 3)
    {
        i = atoi(argv[1]);
        j = atoi(argv[2]);
        if(!i || !j)
        {
            cout << "Argumentit syötetty väärin!" << endl;
        }
    }
    if (argc < 3 || (!i || !j))
    {
        cout << "Syötä rivien lukumäärä kokonaislukuna: ";
        cin >> i;
        while (!cin)
        {
            cin.clear();
            cin.ignore(999, '\n');
            cout << "Syötä rivien lukumäärä kokonaislukuna: ";
            cin >> i;
        }
        cout << "Syötä sarakkeiden lukumäärä kokonaislukuna: ";
        cin >> j;
        while (!cin)
        {
            cin.clear();
            cin.ignore(999, '\n');
            cout << "Syötä sarakkeiden lukumäärä kokonaislukuna: ";
            cin >> j;
        }
    }
    Matrix *matrix = new Matrix(i, j);
    matrix->printmatrix();
    delete matrix;
    return 0;
}
