#ifndef MATRIX_H
#define MATRIX_H
#include <string>
#include <iostream>
#include <vector>

using namespace std;

//!  Matrix class
/*!
  Includes variables for rows and columns and function to print elements of the matrix.
*/

class Matrix
{
    public:
        //! Default constructor.
        Matrix();
        //! Constructor.
        /*!
        \param rows - an integer argument
        \param columns - an integer argument
        */
        Matrix(int rows, int columns);
        //! Setter for rows
        /*!
        \param rows - an integer argument
        */
        void setRows(int rows);
        //! Setter for columns
        /*!
        \param columns - an integer argument
         */
        void setColumns(int columns);
        //! printmatrix function
        /*! Prints out (rows * columns) matrix elements where elements are indexes of the matrix. */
        void printmatrix() const;
        //! Destructor.
        virtual ~Matrix();
        int rows; /*!< Amount of rows given as integer argument or by the user. */
        int columns; /*!< Amount of columns given as integer argument or by the user. */
    protected:
    private:
};

#endif // MATRIX_H
