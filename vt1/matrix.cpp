#include "matrix.h"
#include <string>
#include <iostream>

using namespace std;

Matrix::Matrix()
{
}

Matrix::Matrix(int rows, int columns)
{
    setRows(rows);
    setColumns(columns);
}

Matrix::~Matrix()
{
}

void Matrix::setRows(int matrows)
{
    rows = matrows;
}

void Matrix::setColumns(int matcolumns)
{
    columns = matcolumns;
}

void Matrix::printmatrix() const
{
    int mat[rows+1][columns+1];
    for(int i = 1; i < rows+1; i++)
    {
        cout << endl;
        for(int j = 1; j < columns+1; j++)
        {
            string index = to_string(i) + to_string(j);
            int index_int = stoi(index);
            mat[i][j] = index_int;
            cout << mat[i][j] << " ";
        }
    }
}
