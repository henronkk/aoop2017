#ifndef SQUAREMATRIX_H
#define SQUAREMATRIX_H

/** \file squarematrix.h */
//! Class to create a squarematrix
class squarematrix
{
    public:
        //! Constructor.
        squarematrix();
        //! Destructor.
        virtual ~squarematrix();
    protected:
    private:
};

#endif // SQUAREMATRIX_H
