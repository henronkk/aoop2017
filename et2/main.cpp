#include <iostream>
#include "greet.h"

using namespace std;

int main()
{
    HelloGreeter *hellogreeter = new HelloGreeter();
    MoroGreeter *morogreeter = new MoroGreeter();
    cout << hellogreeter->sayHello() << endl;
    cout << morogreeter->sayHello() << endl;
    delete hellogreeter;
    delete morogreeter;

}
