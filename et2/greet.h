#ifndef GREET_H
#define GREET_H
#include <string>

using namespace std;

//! Includes saying greetings to the world in english.
class HelloGreeter
{
    public:
        //! Constructor.
        HelloGreeter();
        //! Destructor.
        virtual ~HelloGreeter();
        //! Says greetings in english.
        /*!
        \return greetings - a string
         */
        string sayHello();
    protected:
    private:
        string greetings = "Hello world!"; /*!< Greetings in english. */
};


//! Includes saying greetings to the world in finnish.
class MoroGreeter
{
    public:
        //! Constructor.
        MoroGreeter();
        //! Destructor.
        virtual ~MoroGreeter();
        //! Says greetings in finnish.
        /*!
        \return greetings - a string
         */
        string sayHello();
    protected:
    private:
        string greetings = "Moro maailma!"; /*!< Greetings in finnish. */
};

#endif // GREET_H
