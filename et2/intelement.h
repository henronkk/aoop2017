#ifndef INTELEMENT_H
#define INTELEMENT_H

/** \file intelement.h */
//! Includes intelement class
class intelement
{
    public:
        //! Constructor.
        intelement();
        //! Destructor.
        virtual ~intelement();
    protected:
    private:
};

#endif // INTELEMENT_H
