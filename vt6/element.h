#ifndef ELEMENT_H
#define ELEMENT_H
#include <iostream>
#include <vector>
#include <memory>
#include <map>

/** \file element.h */
using Valuation = std::map< char, int >;
extern Valuation val;

//! Includes Element base class
class Element
{
    public:
        //! Destructor
        virtual ~Element() {};
        //! Clone function
        /*!
        \return shared_ptr<Element>
        */
        virtual std::shared_ptr<Element> clone() const = 0;
        //! toString function
        /*!
        returns the string representation of Element
        \return string
        */
        virtual std::string toString() const = 0;
        //! evaluate function
        /*!
        \param val - const Valuation&
        \return int
        */
        virtual int evaluate(const Valuation& val) const = 0;
};

std::ostream& operator<<(std::ostream& os, const Element& i);

//! Includes IntElement class
class IntElement : public Element
{
    public:
        //! Default Constructor
        IntElement();
        //! Constructor
        /*!
        \param v - int
         */
        explicit IntElement(int v);
        //! Destructor.
        virtual ~IntElement();
        //! Value Getter
        /*!
        \return int
        */
        int getVal() const;
        //! Value Setter
        /*!
        \param v - int
        */
        void setVal(int v);
        //! Clone function
        /*!
        returns the pointer to a copy of this
        \return IntElement*
        */
        std::shared_ptr<Element> clone() const;
        //! toString function
        /*!
        returns the string representation of val
        \return string
        */
        std::string toString() const;
        //! evaluate function
        /*!
        returns val
        \param val - const Valuation&
        \return int
        */
        int evaluate(const Valuation& val) const;
        //! Add AND assignment operator
        /*!
        \param i - IntElement
        \return IntElement&
        */
        IntElement& operator+=(const IntElement& i);
        //! Subtract AND assignment operator
        /*!
        \param i - const IntElement&
        \return IntElement&
        */
        IntElement& operator-=(const IntElement& i);
        //! Multiply AND assignment operator
        /*!
        \param i - const IntElement&
        \return IntElement&
        */
        IntElement& operator*=(const IntElement& i);
        //! Add operator
        /*!
        \param i - const IntElement&
        \return IntElement
        */
        IntElement operator+(const IntElement& i);
        //! Subtract operator
        /*!
        \param i - const IntElement&
        \return IntElement
        */
        IntElement operator-(const IntElement& i);
        //! Multiply operator
        /*!
        \param i - const IntElement&
        \return IntElement
        */
        IntElement operator*(const IntElement& i);
        //! Equal operator
        /*!
        \param m - const IntElement&
        \return bool
        */
        bool operator==(const IntElement& i) const;
        //! Print operator
        /*!
        \param os - ostream&
        \param i - const IntElement&
        \return ostream&
        */
        friend std::ostream& operator<<(std::ostream& os, const IntElement& i);
    protected:
    private:
        int val; /*!< Value of matrix element */
};

//! Includes VariableElement class
class VariableElement : public Element
{
    public:
        //! Default Constructor
        VariableElement();
        //! Constructor
        /*!
        \param v - char
         */
        explicit VariableElement(char v);
        //! Destructor.
        virtual ~VariableElement();
        //! Value Getter
        /*!
        \return const char
        */
        char getVal() const;
        //! Value Setter
        /*!
        \param v - char
        */
        void setVal(char v);
        //! Clone function
        /*!
        returns the pointer to a copy of this
        \return VariableElement*
        */
        std::shared_ptr<Element> clone() const;
        //! toString function
        /*!
        returns the string representation of var
        \return string
        */
        std::string toString() const;
        //! evaluate function
        /*!
        returns val[var] or throws an exception
        \param val - const Valuation&
        \return int
        */
        int evaluate(const Valuation& val) const;
        //! Equal operator
        /*!
        \param m - const VariableElement&
        \return bool
        */
        bool operator==(const VariableElement& i) const;
        //! Print operator
        /*!
        \param os - ostream&
        \param i - const VariableElement&
        \return ostream&
        */
        friend std::ostream& operator<<(std::ostream& os, const VariableElement& i);
    protected:
    private:
        char var; /*!< Variable of matrix element */
};

#endif // ELEMENT_H
