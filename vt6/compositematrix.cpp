#define CATCH_CONFIG_MAIN
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <cstring>
#include <functional>
#include "compositematrix.h"
#include "symbolicmatrix.h"
#include "catch.hpp"

/** \file compositematrix.cpp */

//! Addition function
/*!
        \param op1 - const ConcreteSquareMatrix&
        \param op2 - const ConcreteSquareMatrix&
        \return ConcreteSquareMatrix
*/
ConcreteSquareMatrix mat_add(const ConcreteSquareMatrix& op1, const ConcreteSquareMatrix& op2)
{
    ConcreteSquareMatrix add1 = op1;
    ConcreteSquareMatrix add2 = op2;
    ConcreteSquareMatrix add_result = (add1+=add2);
    return add_result;
}

//! Negation function
/*!
        \param op1 - const ConcreteSquareMatrix&
        \param op2 - const ConcreteSquareMatrix&
        \return ConcreteSquareMatrix
*/
ConcreteSquareMatrix mat_neg(const ConcreteSquareMatrix& op1, const ConcreteSquareMatrix& op2)
{
    ConcreteSquareMatrix neg1 = op1;
    ConcreteSquareMatrix neg2 = op2;
    ConcreteSquareMatrix neg_result = (neg1-=neg2);
    return neg_result;
}

//! Multiplication function
/*!
        \param op1 - const ConcreteSquareMatrix&
        \param op2 - const ConcreteSquareMatrix&
        \return ConcreteSquareMatrix
*/
ConcreteSquareMatrix mat_mul(const ConcreteSquareMatrix& op1, const ConcreteSquareMatrix& op2)
{
    ConcreteSquareMatrix mul1 = op1;
    ConcreteSquareMatrix mul2 = op2;
    ConcreteSquareMatrix mul_result = (mul1*=mul2);
    return mul_result;
}

Valuation val = {{'a',1},{'b',2},{'c',3},{'d',4},{'x',2},{'y',3}};
Valuation key_val_pairs = {{'a',1},{'b',2},{'c',3},{'x',2},{'y',3}};
Valuation fake_keys = {{'f',1},{'g',2}};

TEST_CASE( "Valuation map test", "[valuation]" )
{
    REQUIRE( key_val_pairs.at('x') == 2 );
    REQUIRE( key_val_pairs.at('y') == 3 );
    REQUIRE_THROWS( key_val_pairs.at('d') == 0 );
    REQUIRE_THROWS( key_val_pairs.at('e') == 10 );
}

TEST_CASE( "Element class tests", "[element]" )
{
    IntElement elem(1);
    IntElement elem2(2);
    VariableElement velem('x');
    VariableElement velem2('y');
    REQUIRE( elem.toString() == "1" );
    REQUIRE( velem.toString() == "x" );
    elem.setVal(2);
    velem.setVal('y');
    REQUIRE( elem.getVal() == 2);
    REQUIRE( velem.getVal() == 'y');
    REQUIRE( (elem == elem2) == true );
    REQUIRE( (velem == velem2) == true );
    REQUIRE( elem.clone()->evaluate(key_val_pairs) == elem2.clone()->evaluate(key_val_pairs) );
    REQUIRE( velem.clone()->evaluate(key_val_pairs) == velem2.clone()->evaluate(key_val_pairs) );
    REQUIRE( (elem.getVal() + elem2.getVal()) == 4 );
    REQUIRE( (elem.getVal() - elem2.getVal()) == 0 );
    REQUIRE( (elem.getVal() * elem2.getVal()) == 4 );
    REQUIRE( (elem.evaluate(key_val_pairs)) == 2);
    REQUIRE( (velem.evaluate(key_val_pairs)) == 3);
    REQUIRE_THROWS( (velem.evaluate(fake_keys)) );
    REQUIRE_NOTHROW( (elem.evaluate(fake_keys)) );
}

TEST_CASE( "SymbolicMatrix test case", "[symbolicmatrix]" )
{
    std::string symb_str    =   "[[1,x][y,4]]";
    std::string symb_str2   =   "[[a,b][c,d]]";
    REQUIRE_NOTHROW(SymbolicSquareMatrix symb(symb_str));
    REQUIRE_NOTHROW(SymbolicSquareMatrix symb2(symb_str2));
    SymbolicSquareMatrix symb(symb_str);
    SymbolicSquareMatrix symb2(symb_str2);
    symb.print(std::cout);
    std::cout << "\n";
    symb2.print(std::cout);
    std::cout << "\n";
    REQUIRE( SymbolicSquareMatrix(symb).toString() == symb.toString() );
    REQUIRE( SymbolicSquareMatrix(symb2).toString() == symb2.toString() );
    REQUIRE( symb.transpose().toString() == "[[1,y][x,4]]");
    REQUIRE( symb2.transpose().toString() == "[[a,c][b,d]]");
    REQUIRE( (symb == symb2) == true );
    REQUIRE_NOTHROW( (symb.evaluate(key_val_pairs)) );
    REQUIRE_THROWS( (symb2.evaluate(key_val_pairs)) );
    REQUIRE( (symb.evaluate(key_val_pairs)).toString() == "[[1,2][3,4]]" );
    REQUIRE_THROWS( (symb2.evaluate(key_val_pairs)).toString() == "[[1,2][3,4]]" );
}

TEST_CASE( "ConcreteMatrix test case", "[concretematrix]" )
{
    std::string con_string = "[[1,2][3,4]]";
    std::string con_string2 = "[[0,0][0,0]]";
    std::string con_string3 = "[[3,4][5,6]]";
    std::string con_string4 = "[[4,5][6,7]]";
    std::string con_string5 = "[[3,3,3][3,3,3][3,3,3]]";
    ConcreteSquareMatrix sqmat(con_string);
    ConcreteSquareMatrix sqmat2(con_string2);
    ConcreteSquareMatrix sqmat3(con_string3);
    ConcreteSquareMatrix sqmat4(con_string4);
    ConcreteSquareMatrix sqmat5(con_string5);
    ConcreteSquareMatrix sqmat_ran(3);
    ConcreteSquareMatrix sqmat_ran2(3);
    REQUIRE_NOTHROW(sqmat_ran.print(std::cout));
    REQUIRE_NOTHROW(sqmat_ran2.print(std::cout));
    REQUIRE_NOTHROW(sqmat_ran.toString());
    REQUIRE_NOTHROW(sqmat_ran2.toString());
    REQUIRE_NOTHROW(sqmat_ran2.clone());
    sqmat2+=sqmat; // Addition
    sqmat3-=sqmat2; // Subtraction
    sqmat4*=sqmat2; // Multiplication
    ConcreteSquareMatrix sqmat6(sqmat.transpose()); // Transpose
    sqmat.print(std::cout);
    std::cout << "\n";
    sqmat2.print(std::cout);
    std::cout << "\n";
    sqmat3.print(std::cout);
    std::cout << "\n";
    sqmat4.print(std::cout);
    std::cout << "\n";
    sqmat6.print(std::cout);
    std::cout << "\n";
    REQUIRE( (sqmat.evaluate(val) == sqmat));
    REQUIRE( (sqmat2==sqmat3) == true ); // Equality test true
    REQUIRE( (sqmat==sqmat5) == false ); // Equality test false
    REQUIRE( sqmat2.toString() == "[[1,2][3,4]]" );
    REQUIRE( sqmat3.toString() == "[[2,2][2,2]]" );
    REQUIRE( sqmat4.toString() == "[[19,28][27,40]]" );
    REQUIRE( sqmat5.toString() == "[[3,3,3][3,3,3][3,3,3]]" );
    REQUIRE( sqmat6.toString() == "[[1,3][2,4]]" );
    ConcreteSquareMatrix sqmat7 = sqmat5;
    REQUIRE( sqmat7.toString() == sqmat5.toString() );
}

TEST_CASE( "CompositeMatrix test case", "[compositematrix]" )
{
    std::string symb_str    =   "[[1,x][y,4]";
    std::string symb_str2   =   "[[a,b][c,d]]";
    std::string con_string = "[[1,2][3,4]]";
    std::string con_string2 = "[[0,0][0,0]]";
    SymbolicSquareMatrix symb(symb_str);
    SymbolicSquareMatrix symb2(symb_str2);
    ConcreteSquareMatrix sqmat(con_string);
    ConcreteSquareMatrix sqmat2(con_string2);
    ConcreteSquareMatrix sqmat_ran(10);
    ConcreteSquareMatrix sqmat_ran2(10);
    CompositeSquareMatrix comp_mat(symb,sqmat,mat_add,'+');
    CompositeSquareMatrix comp_mat2(symb2,sqmat2,mat_neg,'-');
    CompositeSquareMatrix comp_mat3(sqmat_ran,sqmat_ran2,mat_mul,'*');
    CompositeSquareMatrix comp_mat4(comp_mat);
    REQUIRE(comp_mat.toString() == comp_mat4.toString());
    REQUIRE_NOTHROW(comp_mat.clone());
    REQUIRE( comp_mat2.evaluate(val) == sqmat);
    REQUIRE( comp_mat.toString() == "[[2,4][6,8]]" );
    REQUIRE( comp_mat2.toString() == "[[1,2][3,4]]" );
}

TEST_CASE( "SquareMatrix test case", "[squarematrix]" )
{
    std::string symb_str   =   "[[a,b][c,d]]";
    std::string con_string =    "[[1,2][3,4]]";
    SymbolicSquareMatrix symb(symb_str);
    ConcreteSquareMatrix sqmat(con_string);
    CompositeSquareMatrix comp_mat(symb,sqmat,mat_add,'+');
    CompositeSquareMatrix comp_mat2(comp_mat);
}

//--------TEST CASES END----------------TEST CASES END----------------TEST CASES END
CompositeSquareMatrix::CompositeSquareMatrix()
{

}

CompositeSquareMatrix::CompositeSquareMatrix(const SquareMatrix& op1, const SquareMatrix& op2,
                                const std::function<ConcreteSquareMatrix(const ConcreteSquareMatrix&,
                                const ConcreteSquareMatrix&)>& opr, char opc)
{
    oprnd1 = op1.clone();
    oprnd2 = op2.clone();
    oprtor = opr;
    oprtor(op1.evaluate(val),op2.evaluate(val));
    op_char = opc;
}

CompositeSquareMatrix::CompositeSquareMatrix(const CompositeSquareMatrix& m)
{
    oprnd1 = m.oprnd1;
    oprnd2 = m.oprnd2;
    oprtor = m.oprtor;
    op_char = m.op_char;
}

CompositeSquareMatrix::CompositeSquareMatrix(CompositeSquareMatrix&& m)
{
    oprnd1 = m.oprnd1;
    oprnd2 = m.oprnd2;
    oprtor = m.oprtor;
    op_char = m.op_char;
}
CompositeSquareMatrix::~CompositeSquareMatrix()
{
    //destructor
}
CompositeSquareMatrix& CompositeSquareMatrix::operator=(const CompositeSquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    oprnd1 = m.oprnd1;
    oprnd2 = m.oprnd2;
    oprtor = m.oprtor;
    op_char = m.op_char;
    return *this;
}

CompositeSquareMatrix& CompositeSquareMatrix::operator=(CompositeSquareMatrix&& m)
{
    if (this == &m)
        {
            return *this;
        }
    oprnd1 = std::move(m.oprnd1);
    oprnd2 = std::move(m.oprnd2);
    oprtor = std::move(m.oprtor);
    op_char = std::move(m.op_char);
    return *this;
}

std::shared_ptr<SquareMatrix> CompositeSquareMatrix::clone() const
{
    std::shared_ptr<SquareMatrix> cloned(new CompositeSquareMatrix(*this));
    return cloned;
}

void CompositeSquareMatrix::print(std::ostream& os) const
{
    os << toString();
}

std::string CompositeSquareMatrix::toString() const
{
    return oprtor(oprnd1->evaluate(val),oprnd2->evaluate(val)).toString();
}

ConcreteSquareMatrix CompositeSquareMatrix::evaluate(const Valuation& val) const
{
    return oprtor(oprnd1->evaluate(val),oprnd2->evaluate(val));
}
