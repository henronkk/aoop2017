#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <cstring>
#include <math.h>
#include "symbolicmatrix.h"
/** \file symbolicmatrix.cpp */

SymbolicSquareMatrix::SymbolicSquareMatrix()
{

}

SymbolicSquareMatrix::SymbolicSquareMatrix(const std::string& str_m)
{
    std::stringstream ss{str_m};
    std::vector<int> values;
    std::vector<int> checker;
    char c;
    int v;
    int p = 0;
    ss >> c;
    ss >> c;
    while(c != ']')
    {
        while(c != ']')
        {
            if (isdigit(ss.peek()))
            {
                ss >> v;
                values.push_back(v);
                checker.push_back(1);
            }
            if (isalpha(ss.peek()))
            {
                ss >> c;
                values.push_back(c);
                checker.push_back(0);
            }
            ss >> c;
        }
        ss >> c;
    }
    n = sqrt(values.size());
    for (int i = 0; i < n; i++)
    {
        std::vector<std::shared_ptr<Element>> col;
        for (int j = 0; j < n; j++)
        {
            if (checker[p] == 1)
            {
                std::shared_ptr<IntElement> numb(new IntElement(values[p]));
                col.push_back(std::move(numb));
                p++;
            }
            else
            {
                std::shared_ptr<VariableElement> varb(new VariableElement(char(values[p])));
                col.push_back(std::move(varb));
                p++;
            }
        }
        elements.push_back(std::move(col));
    }
}

SymbolicSquareMatrix::SymbolicSquareMatrix(const SymbolicSquareMatrix& m)
{
    n = m.n;
    elements = m.elements;
}

SymbolicSquareMatrix::SymbolicSquareMatrix(SymbolicSquareMatrix&& m)
{
    n = m.n;
    elements = m.elements;
}

SymbolicSquareMatrix::~SymbolicSquareMatrix()
{
    //std::cout << "Destructor called" << std::endl;
}

SymbolicSquareMatrix& SymbolicSquareMatrix::operator=(const SymbolicSquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = m.n;
    elements = m.elements;
    return *this;
}

SymbolicSquareMatrix& SymbolicSquareMatrix::operator=(SymbolicSquareMatrix&& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = std::move(m.n);
    elements = std::move(m.elements);
    return *this;
}

std::shared_ptr<SquareMatrix> SymbolicSquareMatrix::clone() const
{
    std::shared_ptr<SquareMatrix> cloned(new SymbolicSquareMatrix(*this));
    return cloned;
}

SymbolicSquareMatrix SymbolicSquareMatrix::transpose() const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[j][i]->toString();
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    SymbolicSquareMatrix tmp(os.str());
    return tmp;
}

bool SymbolicSquareMatrix::operator==(const SymbolicSquareMatrix& m) const
{
    return elements.size() == m.elements.size();
}

void SymbolicSquareMatrix::print(std::ostream& os) const
{
    os << toString();
}

std::string SymbolicSquareMatrix::toString() const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[i][j]->toString();
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    return os.str();
}
ConcreteSquareMatrix SymbolicSquareMatrix::evaluate(const Valuation& val) const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[i][j]->evaluate(val);
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    ConcreteSquareMatrix tmp(os.str());
    return tmp;
}
