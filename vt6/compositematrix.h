#ifndef COMPOSITEMATRIX_H
#define COMPOSITEMATRIX_H
#include "squarematrix.h"
#include "concretematrix.h"

/** \file compositematrix.h */
//! Class for CompositeSquareMatrix
class CompositeSquareMatrix : public SquareMatrix
{
    public:
        //! Default Constructor
        CompositeSquareMatrix();
        //! Constructor
        /*!
        \param op1 - const SquareMatrix&
        \param op2 - const SquareMatrix&
        \param opr - const std::function<CSM(const CSM&, const CSM&)>&
        \param opc - char
        */
        CompositeSquareMatrix(const SquareMatrix& op1, const SquareMatrix& op2,
                                const std::function<ConcreteSquareMatrix(const ConcreteSquareMatrix&,
                                const ConcreteSquareMatrix&)>& opr, char opc);
        //! Copy constructor
        /*!
        \param m - const CompositeSquareMatrix&
        */
        CompositeSquareMatrix(const CompositeSquareMatrix& m);
        //! Transfer copy constructor
        /*!
        \param m - CompositeSquareMatrix&&
        */
        CompositeSquareMatrix(CompositeSquareMatrix&& m);
        //! Destructor
        virtual ~CompositeSquareMatrix();
        //! Assignment operator
        /*!
        \param m - const CompositeSquareMatrix&
        \return CompositeSquareMatrix&
        */
        CompositeSquareMatrix& operator=(const CompositeSquareMatrix& m);
        //! Move assignment operator
        /*!
        \param m - CompositeSquareMatrix&&
        \return CompositeSquareMatrix&
        */
        CompositeSquareMatrix& operator=(CompositeSquareMatrix&& m);
        //! Clone function
        /*!
        \return shared_ptr<SquareMatrix>
        */
        std::shared_ptr<SquareMatrix> clone() const;
        //! Print function
        /*!
        returns os << *oprnd1 << op_char << *oprnd2
        \param os - ostream&
        \return ostream
        */
        void print(std::ostream &os) const;
        //! toString function
        /*!
        returns string representation of the matrix
        \return string
        */
        std::string toString() const;
        //! evaluate function
        /*!
        returns oprtor(oprnd1->evaluate(val),oprnd2->evaluate(val))
        \param val - const Valuation&
        \return oprtor(oprnd1->evaluate(val),oprnd2->evaluate(val))
        */
        ConcreteSquareMatrix evaluate(const Valuation& val) const override;
    protected:
    private:
        std::shared_ptr<SquareMatrix> oprnd1; /*!< Operand 1 */
        std::shared_ptr<SquareMatrix> oprnd2; /*!< Operand 2 */
        std::function<ConcreteSquareMatrix(const ConcreteSquareMatrix&, const ConcreteSquareMatrix&)> oprtor; /*!< CSM operator function */
        char op_char; /*!< Operator symbol such as + - or * */

};

#endif // COMPOSITEMATRIX_H
