#ifndef SQUAREMATRIX_H
#define SQUAREMATRIX_H
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include "element.h"

class CompositeSquareMatrix;
class SymbolicSquareMatrix;
class ConcreteSquareMatrix;

/** \file squarematrix.h */
//! SquareMatrix base class
class SquareMatrix
{
    public:
        //! Destructor
        virtual ~SquareMatrix() {};
        //! Clone function
        /*!
        \return shared_ptr<SquareMatrix>
        */
        virtual std::shared_ptr<SquareMatrix> clone() const = 0;
        //! toString function
        /*!
        returns the string representation of SquareMatrix
        \return string
        */
        virtual std::string toString() const = 0;
        //! evaluate function
        /*!
        \param val - const Valuation&
        \return ConcreteSquareMatrix
        */
        virtual ConcreteSquareMatrix evaluate(const Valuation& val) const = 0;
    protected:
    private:
};

#endif // SQUAREMATRIX_H
