#ifndef CONCRETEMATRIX_H
#define CONCRETEMATRIX_H
#include <iostream>
#include <vector>
#include <memory>
#include "element.h"
#include "symbolicmatrix.h"

/*! \mainpage VT4 documentation */
/** \file concretematrix.h */
//! Class to create a ConcreteSquareMatrix
class ConcreteSquareMatrix
{
    public:
        //! Default Constructor
        ConcreteSquareMatrix();
        //! Constructor
        /*!
        Constructs a matrix from the string representation of the form [[1,2][3,4]]
        \param str_m - const std::string&
        */
        explicit ConcreteSquareMatrix(const std::string& str_m);
        //! Copy constructor
        /*!
        \param m - const ConcreteSquareMatrix&
        */
        ConcreteSquareMatrix(const ConcreteSquareMatrix& m);
        //! Transfer copy constructor
        /*!
        \param m - ConcreteSquareMatrix&&
        */
        ConcreteSquareMatrix(ConcreteSquareMatrix&& m);
        //! Destructor
        virtual ~ConcreteSquareMatrix();
        //! Assignment operator
        /*!
        \param m - const ConcreteSquareMatrix&
        \return ConcreteSquareMatrix&
        */
        ConcreteSquareMatrix& operator=(const ConcreteSquareMatrix& m);
        //! Move assignment operator
        /*!
        \param m - ConcreteSquareMatrix&&
        \return ConcreteSquareMatrix&
        */
        ConcreteSquareMatrix& operator=(ConcreteSquareMatrix&& m);
        //! Transpose matrix
        /*!
        \return ConcreteSquareMatrix
        */
        ConcreteSquareMatrix transpose() const;
        //! Addition operator
        /*!
        \param m - const ConcreteSquareMatrix&
        \return ConcreteSquareMatrix&
        */
        ConcreteSquareMatrix& operator+=(const ConcreteSquareMatrix& m);
        //! Negation operator
        /*!
        \param m - const ConcreteSquareMatrix&
        \return ConcreteSquareMatrix&
        */
        ConcreteSquareMatrix& operator-=(const ConcreteSquareMatrix& m);
        //! Multiplication operator
        /*!
        \param m - const ConcreteSquareMatrix&
        \return ConcreteSquareMatrix&
        */
        ConcreteSquareMatrix& operator*=(const ConcreteSquareMatrix& m);
        //! Equal operator
        /*!
        \param m - const ConcreteSquareMatrix&
        \return bool
        */
        bool operator==(const ConcreteSquareMatrix& m) const;
        //! Print function
        /*!
        Outputs the matrix into the stream os in the form [[1,2],[3,4]]
        \param os - ostream&
        \return ostream
        */
        void print(std::ostream &os) const;
        //! toString function
        /*!
        Returns string of the output in the form [[1,2],[3,4]]
        \return string
        */
        std::string toString() const;
    protected:
    private:
        unsigned int n; /*!< Dimensions of the square matrix. */
        std::vector<std::vector<std::shared_ptr<IntElement>>> elements; /*!< Elements of the square matrix inside 2D vector. */
};

#endif // CONCRETEMATRIX_H
