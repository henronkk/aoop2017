#include "greet.h"
/*! \file greet.cpp */
Greeter::~Greeter()
{
    //nothing here
}

const std::string& Greeter::greet() const
{
    static const std::string val;
    return val;
}

HelloGreeter::HelloGreeter()
{
    //nothing here
}

HelloGreeter::~HelloGreeter()
{
    //nothing here
}

MoroGreeter::MoroGreeter()
{
    //nothing here
}

MoroGreeter::~MoroGreeter()
{
    //nothing here
}
