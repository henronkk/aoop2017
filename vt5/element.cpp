#include <iostream>
#include <vector>
#include <string>
#include "element.h"

/** \file element.cpp */
Element::~Element()
{
    //std::cout << "Element destructor called" << std::endl;
}

std::shared_ptr<Element> Element::clone() const
{
   // clone element to pointer
}

std::string Element::toString() const
{
    // return element as string
}

int Element::evaluate(const Valuation& val) const
{
    // evaluate var to val
}
//------------------------------------------------
IntElement::IntElement()
{

}
IntElement::IntElement(int v)
{
    setVal(v);
}

IntElement::~IntElement()
{
    //std::cout << "IntElement destructor called" << std::endl;
}

int IntElement::getVal() const {
    return val;
}

void IntElement::setVal(int v) {
    val = v;
}

std::shared_ptr<Element> IntElement::clone() const
{
    std::shared_ptr<Element> cloned(new IntElement(this->getVal()));
    return cloned;
}

std::string IntElement::toString() const
{
    std::string str = std::to_string(val);
    return str;
}

int IntElement::evaluate(const Valuation& val) const
{
    return this->val;
}

IntElement& IntElement::operator+=(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    val += i.val;
    return *this;
}

IntElement& IntElement::operator-=(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    val -= i.val;
    return *this;
}

IntElement& IntElement::operator*=(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    val *= i.val;
    return *this;
}

IntElement IntElement::operator+(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    return IntElement(val + i.val);
}

IntElement IntElement::operator-(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    return IntElement(val - i.val);
}

IntElement IntElement::operator*(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    return IntElement(val * i.val);
}

bool IntElement::operator==(const IntElement& i) const
{
    return val == i.val;
}

std::ostream& operator<<(std::ostream& os, const IntElement& i)
{
    os << i.val;
    return os;
}
//------------------------------------------------
VariableElement::VariableElement()
{

}

VariableElement::VariableElement(char v)
{
    setVal(v);
}

VariableElement::~VariableElement()
{
    //std::cout << "VariableElement destructor called" << std::endl;
}

char VariableElement::getVal() const
{
    return var;
}

void VariableElement::setVal(char v) {
    var = v;
}

std::shared_ptr<Element> VariableElement::clone() const
{
    std::shared_ptr<Element> cloned(new VariableElement(this->getVal()));
    return cloned;
}

std::string VariableElement::toString() const
{
    // return var as a string
    std::string str;
    str.push_back(var);
    return str;
}

int VariableElement::evaluate(const Valuation& val) const
{
    std::map< char,int >::const_iterator it;
    try
    {
        return val.at(this->var);
    }
    catch (std::out_of_range& err)
    {
        throw std::out_of_range("That key does not have value!");
    }
}

bool VariableElement::operator==(const VariableElement& i) const
{
    return var == i.var;
}

std::ostream& operator<<(std::ostream& os, const VariableElement& i)
{
    os << i.var;
    return os;
}
