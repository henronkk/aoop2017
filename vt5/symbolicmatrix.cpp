#define CATCH_CONFIG_MAIN
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <cstring>
#include <math.h>
#include "symbolicmatrix.h"
#include "catch.hpp"
/** \file symbolicmatrix.cpp */

Valuation key_val_pairs = {{'a',1},{'b',2},{'c',3},{'x',2},{'y',3}};

TEST_CASE( "Valuation map test", "[valuation]" )
{
    REQUIRE( key_val_pairs.at('x') == 2 );
    REQUIRE( key_val_pairs.at('y') == 3 );
    REQUIRE_THROWS( key_val_pairs.at('d') == 0 );
    REQUIRE_THROWS( key_val_pairs.at('e') == 10 );
}

TEST_CASE( "Element class tests", "[element]" )
{
    Valuation fake_keys = {{'f',1},{'g',2}};
    IntElement elem(1);
    IntElement elem2(2);
    VariableElement velem('x');
    VariableElement velem2('y');
    REQUIRE( elem.toString() == "1" );
    REQUIRE( velem.toString() == "x" );
    elem.setVal(2);
    velem.setVal('y');
    REQUIRE( elem.getVal() == 2);
    REQUIRE( velem.getVal() == 'y');
    REQUIRE( (elem == elem2) == true );
    REQUIRE( (velem == velem2) == true );
    REQUIRE( elem.clone()->evaluate(key_val_pairs) == elem2.clone()->evaluate(key_val_pairs) );
    REQUIRE( velem.clone()->evaluate(key_val_pairs) == velem2.clone()->evaluate(key_val_pairs) );
    REQUIRE( (elem.getVal() + elem2.getVal()) == 4 );
    REQUIRE( (elem.getVal() - elem2.getVal()) == 0 );
    REQUIRE( (elem.getVal() * elem2.getVal()) == 4 );
    REQUIRE( (elem.evaluate(key_val_pairs)) == 2);
    REQUIRE( (velem.evaluate(key_val_pairs)) == 3);
    REQUIRE_THROWS( (velem.evaluate(fake_keys)) );
    REQUIRE_NOTHROW( (elem.evaluate(fake_keys)) );
}

TEST_CASE( "SymbolicMatrix test case", "[symbolicmatrix]" )
{
    std::string symb_str    =   "[[1,x][y,4]";
    std::string symb_str2   =   "[[a,b][c,d]";
    REQUIRE_NOTHROW(SymbolicSquareMatrix symb(symb_str));
    REQUIRE_NOTHROW(SymbolicSquareMatrix symb2(symb_str2));
    SymbolicSquareMatrix symb(symb_str);
    SymbolicSquareMatrix symb2(symb_str2);
    symb.print(std::cout);
    std::cout << "\n";
    symb2.print(std::cout);
    std::cout << "\n";
    REQUIRE( SymbolicSquareMatrix(symb).toString() == symb.toString() );
    REQUIRE( SymbolicSquareMatrix(symb2).toString() == symb2.toString() );
    REQUIRE( symb.transpose().toString() == "[[1,y][x,4]]");
    REQUIRE( symb2.transpose().toString() == "[[a,c][b,d]]");
    REQUIRE( (symb == symb2) == true );
    REQUIRE_NOTHROW( (symb.evaluate(key_val_pairs)) );
    REQUIRE_THROWS( (symb2.evaluate(key_val_pairs)) );
    REQUIRE( (symb.evaluate(key_val_pairs)).toString() == "[[1,2][3,4]]" );
    REQUIRE_THROWS( (symb2.evaluate(key_val_pairs)).toString() == "[[1,2][3,4]]" );
}

TEST_CASE( "ConcreteMatrix test case", "[concretematrix]" )
{
    std::string con_string = "[[1,2][3,4]]";
    std::string con_string2 = "[[0,0][0,0]]";
    std::string con_string3 = "[[3,4][5,6]]";
    std::string con_string4 = "[[4,5][6,7]]";
    std::string con_string5 = "[[3,3,3][3,3,3][3,3,3]]";
    ConcreteSquareMatrix sqmat(con_string);
    ConcreteSquareMatrix sqmat2(con_string2);
    ConcreteSquareMatrix sqmat3(con_string3);
    ConcreteSquareMatrix sqmat4(con_string4);
    ConcreteSquareMatrix sqmat5(con_string5);
    sqmat2+=sqmat; // Addition
    sqmat3-=sqmat2; // Subtraction
    sqmat4*=sqmat2; // Multiplication
    ConcreteSquareMatrix sqmat6(sqmat.transpose()); // Transpose
    sqmat.print(std::cout);
    std::cout << "\n";
    sqmat2.print(std::cout);
    std::cout << "\n";
    sqmat3.print(std::cout);
    std::cout << "\n";
    sqmat4.print(std::cout);
    std::cout << "\n";
    sqmat6.print(std::cout);
    std::cout << "\n";
    REQUIRE( (sqmat2==sqmat3) == true ); // Equality test true
    REQUIRE( (sqmat==sqmat5) == false ); // Equality test false
    REQUIRE( sqmat2.toString() == "[[1,2][3,4]]" );
    REQUIRE( sqmat3.toString() == "[[2,2][2,2]]" );
    REQUIRE( sqmat4.toString() == "[[19,28][27,40]]" );
    REQUIRE( sqmat5.toString() == "[[3,3,3][3,3,3][3,3,3]]" );
    REQUIRE( sqmat6.toString() == "[[1,3][2,4]]" );
    ConcreteSquareMatrix sqmat7 = sqmat5;
    REQUIRE( sqmat7.toString() == sqmat5.toString() );
}

//---------------------------------------------------------
SymbolicSquareMatrix::SymbolicSquareMatrix()
{

}

SymbolicSquareMatrix::SymbolicSquareMatrix(const std::string& str_m)
{
    std::stringstream ss{str_m};
    std::vector<int> values;
    std::vector<int> checker;
    char c;
    int v;
    int p = 0;
    ss >> c;
    ss >> c;
    while(c != ']')
    {
        while(c != ']')
        {
            if (isdigit(ss.peek()))
            {
                ss >> v;
                values.push_back(v);
                checker.push_back(1);
            }
            if (isalpha(ss.peek()))
            {
                ss >> c;
                values.push_back(c);
                checker.push_back(0);
            }
            ss >> c;
        }
        ss >> c;
    }
    n = sqrt(values.size());
    for (int i = 0; i < n; i++)
    {
        std::vector<std::shared_ptr<Element>> col;
        for (int j = 0; j < n; j++)
        {
            if (checker[p] == 1)
            {
                std::shared_ptr<IntElement> numb(new IntElement(values[p]));
                col.push_back(std::move(numb));
                p++;
            }
            else
            {
                std::shared_ptr<VariableElement> varb(new VariableElement(char(values[p])));
                col.push_back(std::move(varb));
                p++;
            }
        }
        elements.push_back(std::move(col));
    }
}

SymbolicSquareMatrix::SymbolicSquareMatrix(const SymbolicSquareMatrix& m)
{
    n = m.n;
    elements = m.elements;
}

SymbolicSquareMatrix::SymbolicSquareMatrix(SymbolicSquareMatrix&& m)
{
    n = m.n;
    elements = m.elements;
}

SymbolicSquareMatrix::~SymbolicSquareMatrix()
{
    //std::cout << "Destructor called" << std::endl;
}

SymbolicSquareMatrix& SymbolicSquareMatrix::operator=(const SymbolicSquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = m.n;
    elements = m.elements;
    return *this;
}

SymbolicSquareMatrix& SymbolicSquareMatrix::operator=(SymbolicSquareMatrix&& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = std::move(m.n);
    elements = std::move(m.elements);
    return *this;
}

SymbolicSquareMatrix SymbolicSquareMatrix::transpose() const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[j][i]->toString();
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    SymbolicSquareMatrix tmp(os.str());
    return tmp;
}

bool SymbolicSquareMatrix::operator==(const SymbolicSquareMatrix& m) const
{
    return elements.size() == m.elements.size();
}

void SymbolicSquareMatrix::print(std::ostream& os) const
{
    os << toString();
}

std::string SymbolicSquareMatrix::toString() const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[i][j]->toString();
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    return os.str();
}
ConcreteSquareMatrix SymbolicSquareMatrix::evaluate(const Valuation& val) const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[i][j]->evaluate(key_val_pairs);
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    ConcreteSquareMatrix tmp(os.str());
    return tmp;
}
