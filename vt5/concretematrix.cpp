#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <cstring>
#include <math.h>
#include "concretematrix.h"

/** \file concretematrix.cpp */
ConcreteSquareMatrix::ConcreteSquareMatrix()
{

}

ConcreteSquareMatrix::ConcreteSquareMatrix(const std::string& str_m)
{
    std::stringstream ss{str_m};
    std::vector<int> values;
    char c;
    int v;
    int p = 0;
    int num;
    ss >> c;
    ss >> c;
    while(c != ']')
    {
        while(c != ']')
        {
            ss >> v;
            values.push_back(v);
            ss >> c;
        }
        ss >> c;
    }
    n = sqrt(values.size());
    for (int i = 0; i < n; i++)
    {
        std::vector<std::shared_ptr<IntElement>> col;
        for (int j = 0; j < n; j++)
        {
            num = values[p];
            std::shared_ptr<IntElement> numbs(new IntElement(num));
            col.push_back(std::move(numbs));
            p++;
        }
        elements.push_back(std::move(col));
    }
}

ConcreteSquareMatrix::ConcreteSquareMatrix(const ConcreteSquareMatrix& m)
{
    n = m.n;
    elements = m.elements;
}

ConcreteSquareMatrix::ConcreteSquareMatrix(ConcreteSquareMatrix&& m)
{
    n = m.n;
    elements = m.elements;
}

ConcreteSquareMatrix::~ConcreteSquareMatrix()
{
    //std::cout << "Destructor called" << std::endl;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator=(const ConcreteSquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = m.n;
    elements = m.elements;
    return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator=(ConcreteSquareMatrix&& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = std::move(m.n);
    elements = std::move(m.elements);
    return *this;
}

ConcreteSquareMatrix ConcreteSquareMatrix::transpose() const
{
    int tmp[n][n];
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            int x = elements[i][j]->getVal();
            tmp[i][j] = x;
        }
    }
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[j][i]);
        }
    }
    return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator+=(const ConcreteSquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    int tmp[n][n];
    memset(tmp, 0, sizeof(tmp[0][0])*n*n);
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            int x = (elements[i][j]->getVal() + m.elements[i][j]->getVal());
            tmp[i][j] = x;
        }
    }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[i][j]);
        }
    }
    return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator-=(const ConcreteSquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    int tmp[n][n];
    memset(tmp, 0, sizeof(tmp[0][0])*n*n);
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            int x = (elements[i][j]->getVal() - m.elements[i][j]->getVal());
            tmp[i][j] = x;
        }
    }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[i][j]);
        }
    }
    return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator*=(const ConcreteSquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    int tmp[n][n];
    memset(tmp, 0, sizeof(tmp[0][0])*n*n);
    for (int i = 0;i < n;i++)
    {
        for (int j = 0;j < n;j++)
        {
            for(int k = 0;k < n;k++)
            {
                int x = (elements[i][k]->getVal() * m.elements[k][j]->getVal());
                tmp[i][j] += x;
            }
        }
    }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[i][j]);
        }
    }
    return *this;
}

bool ConcreteSquareMatrix::operator==(const ConcreteSquareMatrix& m) const
{
    return elements.size() == m.elements.size();
}

void ConcreteSquareMatrix::print(std::ostream& os) const
{
    os << toString();
}

std::string ConcreteSquareMatrix::toString() const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[i][j]->getVal();
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    return os.str();
}
