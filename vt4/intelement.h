#ifndef INTELEMENT_H
#define INTELEMENT_H
#include <iostream>
#include <vector>
#include <memory>

/*! \mainpage VT4 documentation */
/** \file intelement.h */
//! Includes IntElement class
class IntElement
{
    public:
        //! Default Constructor.
        IntElement();
        //! Constructor
        /*!
        \param v - int
         */
        explicit IntElement(int v);
        //! Destructor.
        virtual ~IntElement();
        //! Value Getter
        /*!
        \return int
        */
        int getVal();
        //! Value Setter
        /*!
        \return value - int
        */
        void setVal(int v);
        //! Clone function
        /*!
        \return IntElement*
        */
        std::shared_ptr<IntElement> clone();
        //! Add AND assignment operator
        /*!
        \param i - IntElement
        \return IntElement&
        */
        IntElement& operator+=(const IntElement& i);
        //! Subtract AND assignment operator
        /*!
        \param i - const IntElement&
        \return IntElement&
        */
        IntElement& operator-=(const IntElement& i);
        //! Multiply AND assignment operator
        /*!
        \param i - const IntElement&
        \return IntElement&
        */
        IntElement& operator*=(const IntElement& i);
        //! Add operator
        /*!
        \param i - const IntElement&
        \return IntElement
        */
        IntElement operator+(const IntElement& i);
        //! Subtract operator
        /*!
        \param i - const IntElement&
        \return IntElement
        */
        IntElement operator-(const IntElement& i);
        //! Multiply operator
        /*!
        \param i - const IntElement&
        \return IntElement
        */
        IntElement operator*(const IntElement& i);
        //! Equal operator
        /*!
        \param m - const SquareMatrix&
        \return bool
        */
        bool operator==(const IntElement& i);
        //! Print operator
        /*!
        \param os - ostream&
        \param i - const IntElement&
        \return ostream&
        */
        friend std::ostream& operator<<(std::ostream& os, const IntElement& i);
    protected:
    private:
        int val; /*!< Value of matrix element */
};
#endif // INTELEMENT_H
