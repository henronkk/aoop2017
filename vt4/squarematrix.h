#ifndef SQUAREMATRIX_H
#define SQUAREMATRIX_H
#include <iostream>
#include <vector>
#include <memory>
#include "intelement.h"

/** \file squarematrix.h */
//! Class to create a SquareMatrix
class SquareMatrix
{
    public:
        //! Default Constructor.
        SquareMatrix();
        //! Constructor
        /*!
        Constructs a matrix from the string representation of the form [[1,2][3,4]]
        \param str_m - const std::string&
        */
        explicit SquareMatrix(const std::string& str_m);
        //! Copy constructor
        /*!
        \param m - const SquareMatrix&
        */
        SquareMatrix(const SquareMatrix& m);
        //! Transfer copy constructor
        /*!
        \param m - SquareMatrix&&
        */
        SquareMatrix(SquareMatrix&& m);
        //! Destructor.
        virtual ~SquareMatrix();
        //! Assignment operator
        /*!
        \param m - const SquareMatrix&
        \return SquareMatrix&
        */
        SquareMatrix& operator=(const SquareMatrix& m);
        //! Move assignment operator
        /*!
        \param m - SquareMatrix&&
        \return SquareMatrix&
        */
        SquareMatrix& operator=(SquareMatrix&& m);
        //! Transpose matrix
        /*!
        \return SquareMatrix
        */
        SquareMatrix transpose() const;
        //! Addition operator
        /*!
        \param m - const SquareMatrix&
        \return SquareMatrix&
        */
        SquareMatrix& operator+=(const SquareMatrix& m);
        //! Negation operator
        /*!
        \param m - const SquareMatrix&
        \return SquareMatrix&
        */
        SquareMatrix& operator-=(const SquareMatrix& m);
        //! Multiplication operator
        /*!
        \param m - const SquareMatrix&
        \return SquareMatrix&
        */
        SquareMatrix& operator*=(const SquareMatrix& m);
        //! Equal operator
        /*!
        \param m - const SquareMatrix&
        \return bool
        */
        bool operator==(const SquareMatrix& m) const;
        //! Print function
        /*!
        Outputs the matrix into the stream os in the form [[1,2],[3,4]]
        \param os - ostream&
        \return ostream
        */
        void print(std::ostream &os) const;
        //! toString function
        /*!
        Returns string of the output in the form [[1,2],[3,4]]
        \return string
        */
        std::string toString() const;
    protected:
    private:
        int n; /*!< Dimensions of the square matrix. */
        std::vector<std::vector<std::shared_ptr<IntElement>>> elements; /*!< Elements of the square matrix inside 2D vector. */
};

#endif // SQUAREMATRIX_H
