#define CATCH_CONFIG_MAIN
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <cstring>
#include <math.h>
#include "catch.hpp"
#include "squarematrix.h"

/** \file squarematrix.cpp */
TEST_CASE( "IntElement initializing & operator testing", "[element]" )
{
    std::cout << "IntElement initializing & operator testing" << std::endl;
    std::cout << ".........................................." << std::endl;
    IntElement element(1);
    IntElement element2(2);
    IntElement element3(3);
    element3.setVal(element.clone()->getVal());
    std::cout << "Testing getter... " << std:: endl;
    REQUIRE( element.getVal() == 1 );
    std::cout << "Testing print-operator..." << std::endl;;
    REQUIRE( std::cout << "Testing element addition...       " << element << "+" << element2 << "=" << element+element2 << std::endl );
    REQUIRE( std::cout << "Testing element subtraction...    " << element << "-" << element2 << "=" << element-element2 << std::endl );
    REQUIRE( std::cout << "Testing element multiplication... " << element << "*" << element2 << "=" << element*element2 << std::endl );
    std::cout << "Testing cloning..." << std::endl;
    REQUIRE( (element3 == element) == true );
}
TEST_CASE( "SquareMatrix initializing & operator testing", "[sqmat]" )
{
    std::cout << "\nSquareMatrix initializing & operator testing" << std::endl;
    std::cout << "............................................" << std::endl;
    SquareMatrix sqmat("[[1,2][3,4]]");
    SquareMatrix sqmat2("[[0,0][0,0]]");
    SquareMatrix sqmat3("[[3,4][5,6]]");
    SquareMatrix sqmat4("[[4,5][6,7]]");
    SquareMatrix sqmat5("[[3,3,3][3,3,3][3,3,3]]");
    sqmat2+=sqmat; // Addition
    sqmat3-=sqmat2; // Subtraction
    sqmat4*=sqmat2; // Multiplication
    SquareMatrix sqmat6(sqmat.transpose()); // Transpose
    std::cout << "\nTesting matrix initialization..... ";
    sqmat.print(std::cout);
    std::cout << "\nTesting matrix addition........... ";
    sqmat2.print(std::cout);
    std::cout << "\nTesting matrix subtraction........ ";
    sqmat3.print(std::cout);
    std::cout << "\nTesting matrix multiplication..... ";
    sqmat4.print(std::cout);
    std::cout << "\nTesting matrix transpose.......... ";
    sqmat6.print(std::cout);
    REQUIRE( (sqmat2==sqmat3) == true ); // Equality test true
    REQUIRE( (sqmat==sqmat5) == false ); // Equality test false
    REQUIRE( sqmat2.toString() == "[[1,2][3,4]]" );
    REQUIRE( sqmat3.toString() == "[[2,2][2,2]]" );
    REQUIRE( sqmat4.toString() == "[[19,28][27,40]]" );
    REQUIRE( sqmat5.toString() == "[[3,3,3][3,3,3][3,3,3]]" );
    REQUIRE( sqmat6.toString() == "[[1,3][2,4]]" );
    SquareMatrix sqmat7 = sqmat5;
    REQUIRE( sqmat7.toString() == sqmat5.toString() );
}

SquareMatrix::SquareMatrix()
{
    n = 1;
    elements = {{0}};
}

SquareMatrix::SquareMatrix(const std::string& str_m)
{
    std::stringstream ss{str_m};
    std::vector<int> values;
    char c;
    int v;
    int p = 0;
    int num;
    ss >> c;
    ss >> c;
    while(c != ']')
    {
        while(c != ']')
        {
            ss >> v;
            values.push_back(v);
            ss >> c;
        }
        ss >> c;
    }
    n = sqrt(values.size());
    for (int i = 0; i < n; i++)
    {
        std::vector<std::shared_ptr<IntElement>> col;
        for (int j = 0; j < n; j++)
        {
            num = values[p];
            std::shared_ptr<IntElement> numbs(new IntElement(num));
            col.push_back(std::move(numbs));
            p++;
        }
        elements.push_back(std::move(col));
    }
}

SquareMatrix::SquareMatrix(const SquareMatrix& m)
{
    n = m.n;
    elements = m.elements;
}

SquareMatrix::~SquareMatrix()
{
    //std::cout << "Destructor called" << std::endl;
}

SquareMatrix& SquareMatrix::operator=(const SquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = m.n;
    elements = m.elements;
    return *this;
}

SquareMatrix& SquareMatrix::operator=(SquareMatrix&& m)
{
    if (this == &m)
        {
            return *this;
        }
    n = std::move(m.n);
    elements = std::move(m.elements);
    return *this;
}

SquareMatrix SquareMatrix::transpose() const
{
    int tmp[n][n];
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            int x = elements[i][j]->getVal();
            tmp[i][j] = x;
        }
    }
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[j][i]);
        }
    }
    return *this;
}

SquareMatrix& SquareMatrix::operator+=(const SquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    int tmp[n][n];
    memset(tmp, 0, sizeof(tmp[0][0])*n*n);
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            int x = (elements[i][j]->getVal() + m.elements[i][j]->getVal());
            tmp[i][j] = x;
        }
    }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[i][j]);
        }
    }
    return *this;
}

SquareMatrix& SquareMatrix::operator-=(const SquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    int tmp[n][n];
    memset(tmp, 0, sizeof(tmp[0][0])*n*n);
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            int x = (elements[i][j]->getVal() - m.elements[i][j]->getVal());
            tmp[i][j] = x;
        }
    }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[i][j]);
        }
    }
    return *this;
}

SquareMatrix& SquareMatrix::operator*=(const SquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    int tmp[n][n];
    memset(tmp, 0, sizeof(tmp[0][0])*n*n);
    for (int i = 0;i < n;i++)
    {
        for (int j = 0;j < n;j++)
        {
            for(int k = 0;k < n;k++)
            {
                int x = (elements[i][k]->getVal() * m.elements[k][j]->getVal());
                tmp[i][j] += x;
            }
        }
    }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j]->setVal(tmp[i][j]);
        }
    }
    return *this;
}

bool SquareMatrix::operator==(const SquareMatrix& m) const
{
    return elements.size() == m.elements.size();
}

void SquareMatrix::print(std::ostream& os) const
{
    os << toString();
}

std::string SquareMatrix::toString() const
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < n; i++)
    {
        os << "[";
        for (int j = 0; j < n; j++)
        {
            os << elements[i][j]->getVal();
            if (j < n-1)
            {
                os << ",";
            }
        }
        os << "]";
    }
    os << "]";
    return os.str();
}
