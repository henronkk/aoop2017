#include <iostream>
#include <vector>
#include "intelement.h"

/** \file intelement.cpp */

IntElement::IntElement()
{

}

IntElement::IntElement(int v)
{
    setVal(v);
}

IntElement::~IntElement()
{
    //std::cout << "IntElement destructor called" << std::endl;
}

int IntElement::getVal() {
    return val;
}

void IntElement::setVal(int v) {
    val = v;
}

std::shared_ptr<IntElement> IntElement::clone()
{
    std::shared_ptr<IntElement> cloned(new IntElement(*this));
    return cloned;
}

IntElement& IntElement::operator+=(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    val += i.val;
    return *this;
}

IntElement& IntElement::operator-=(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    val -= i.val;
    return *this;
}

IntElement& IntElement::operator*=(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    val *= i.val;
    return *this;
}

IntElement IntElement::operator+(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    return IntElement(val + i.val);
}

IntElement IntElement::operator-(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    return IntElement(val - i.val);
}

IntElement IntElement::operator*(const IntElement& i)
{
    if (this == &i)
        {
            return *this;
        }
    return IntElement(val * i.val);
}

bool IntElement::operator==(const IntElement& i)
{
    return val == i.val;
}

std::ostream& operator<<(std::ostream& os, const IntElement& i)
{
    os << i.val;
    return os;
}
