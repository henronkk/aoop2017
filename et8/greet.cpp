#include "greet.h"
#include <mutex>
#include <string>
#include <iostream>

/*! \file greet.cpp */
std::mutex mux;
const std::string& HelloGreeter::greet() const
{
    mux.lock();
    std::cout << greetings << std::endl;
    mux.unlock();
    return greetings;
}

const std::string& MoroGreeter::greet() const
{
    mux.lock();
    std::cout << greetings << std::endl;
    mux.unlock();
    return greetings;
}
