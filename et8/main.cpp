#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <memory>
#include <thread>
#include "greet.h"


/*! \file main.cpp */
//! Function to call in case of invalid or missing argument
/*!
    \return amount of prints - int
*/

int invalidArg()
{
    std::cerr << "Invalid arguments!" << std::endl;
    std::cout << "Enter valid integer input: ";
    int val;
    std::cin >> val;
    if (val < 0)
    {
        std::cout << "Input needs to be non-negative!" << std::endl;
        val = invalidArg();
    }
    while (!std::cin)
    {
        std::cin.clear();
        std::cin.ignore(999, '\n');
        std::cout << "Enter valid integer input: ";
        std::cin >> val;
    }
    return val;
}

//! Function to print object
/*!
    // Creates val amount of each object and prints them out to the console.
    \param amount of prints - int
*/
void printObject(int val)
{
    HelloGreeter hello[val];
    MoroGreeter moro[val];
    std::vector<std::thread> t1;
    std::vector<std::thread> t2;
    for(auto& r : hello)
    {
        t1.push_back(std::thread(std::bind(&HelloGreeter::greet, &r)));
    }
    for(auto& r : moro)
    {
        t2.push_back(std::thread(std::bind(&MoroGreeter::greet, &r)));
    }
    for(auto& th : t1)
    {
        th.join();
    }
    for(auto& th : t2)
    {
        th.join();
    }
}

int main(int argc, char* argv[])
{
    int val;
    try
    {
        if (argc == 2)
        {
            int i = std::stoi(argv[1]);
            if (i < 0)
            {
                i = invalidArg();
            }
            printObject(i);
            return 0;
        }
        else
        {
            val = invalidArg();
            if (val < 0)
            {
                val = invalidArg();
            }
            printObject(val);
            return 0;
        }
    }
    catch (std::exception const &exc)
    {
        val = invalidArg();
        printObject(val);
        return 0;
    }
}
