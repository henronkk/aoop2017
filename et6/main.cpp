#include <iostream>
#include <string>
#include <functional>

/*! \file main.cpp */
//! Function to say hello in english
/*!
    \return greetings - std::string
*/
std::string sayHello()
{
    std::string greetings = "Hello world!";
    return greetings;
}
//! Function to say hello in finnish
/*!
    \return greetings - std::string
*/
std::string sayMoro()
{
    std::string greetings = "Moro maailma!";
    return greetings;
}

int main()
{
    std::function<std::string()> hello = sayHello;// = HelloGreeter;
    std::function<std::string()> moro = sayMoro;// = MoroGreeter;
    std::cout << hello() << std::endl;
    std::cout << moro() << std::endl;
}
