#ifndef SYMBOLICMATRIX_H
#define SYMBOLICMATRIX_H
#include "squarematrix.h"
#include "concretematrix.h"
#include "element.h"

/** \file symbolicmatrix.h */
//! Class to create a SymbolicSymbolicSquareMatrix
class SymbolicSquareMatrix : public SquareMatrix
{
    public:
        //! Default Constructor
        SymbolicSquareMatrix();
        //! Constructor
        /*!
        Constructs a matrix from the string representation of the form [[1,x][y,4]]
        \param str_m - const std::string&
        */
        explicit SymbolicSquareMatrix(const std::string& str_m);
        //! Copy constructor
        /*!
        \param m - const SymbolicSquareMatrix&
        */
        SymbolicSquareMatrix(const SymbolicSquareMatrix& m);
        //! Transfer copy constructor
        /*!
        \param m - SymbolicSquareMatrix&&
        */
        SymbolicSquareMatrix(SymbolicSquareMatrix&& m);
        //! Destructor
        virtual ~SymbolicSquareMatrix();
        //! Assignment operator
        /*!
        \param m - const SymbolicSquareMatrix&
        \return SymbolicSquareMatrix&
        */
        SymbolicSquareMatrix& operator=(const SymbolicSquareMatrix& m);
        //! Move assignment operator
        /*!
        \param m - SymbolicSquareMatrix&&
        \return SymbolicSquareMatrix&
        */
        SymbolicSquareMatrix& operator=(SymbolicSquareMatrix&& m);
        //! Clone function
        /*!
        \return shared_ptr<SquareMatrix>
        */
        std::shared_ptr<SquareMatrix> clone() const;
        //! Transpose matrix
        /*!
        \return SymbolicSquareMatrix
        */
        SymbolicSquareMatrix transpose() const;
        //! Equal operator
        /*!
        \param m - const SymbolicSquareMatrix&
        \return bool
        */
        bool operator==(const SymbolicSquareMatrix& m) const;
        //! Print function
        /*!
        Outputs the matrix into the stream os in the form [[1,x],[y,4]]
        \param os - ostream&
        \return ostream
        */
        void print(std::ostream &os) const;
        //! toString function
        /*!
        Returns string of the output in the form [[1,x],[y,4]]
        \return string
        */
        std::string toString() const;
        //! evaluate function
        /*!
        returns an instance of ConcreteSquareMatrix obtained by applying val to each element or throws an exception
        \param val - const Valuation
        \return ConcreteSquareMatrix
        */
        ConcreteSquareMatrix evaluate(const Valuation& val) const;
    protected:
    private:
        unsigned int n; /*!< Dimensions of the square matrix. */
        std::vector<std::vector<std::shared_ptr<Element>>> elements; /*!< Elements of the square matrix inside 2D vector. */
};

#endif // SYMBOLICMATRIX_H
