#define CATCH_CONFIG_MAIN
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <thread>
#include <functional>
#include <mutex>
#include "catch.hpp"
#include "squarematrix.h"

/** \file squarematrix.cpp */

TEST_CASE( "IntElement initializing & operator testing", "[element]" )
{
    std::cout << "IntElement initializing & operator testing" << std::endl;
    std::cout << ".........................................." << std::endl;
    int n_i = 4; //! SquareMatrix size
    IntElement element(1);
    IntElement element2(2);
    std::cout << "Testing getter... " << std:: endl;
    REQUIRE( element.getVal() == 1 );
    std::cout << "Testing print-operator..." << std::endl;;
    REQUIRE( std::cout << "Testing element addition...       " << element << "+" << element2 << "=" << element+element2 << std::endl );
    REQUIRE( std::cout << "Testing element subtraction...    " << element << "-" << element2 << "=" << element-element2 << std::endl );
    REQUIRE( std::cout << "Testing element multiplication... " << element << "*" << element2 << "=" << element*element2 << std::endl );
}
TEST_CASE( "SquareMatrix initializing & operator testing", "[sqmat]" )
{
    std::vector<std::thread> t1;
    std::cout << "\nSquareMatrix initializing & operator testing" << std::endl;
    std::cout << "............................................" << std::endl;
    int n_i = 3; // Squarematrix size
    IntElement element(0);
    std::vector<std::vector<IntElement>> sqmat1(n_i,std::vector<IntElement>(n_i,element));
    for (int i = 0;i < n_i;i++)
    {
        for(int j = 0;j < n_i;j++)
        {
            sqmat1[i][j] = element+i;
        }
    }
    SquareMatrix sqmat2(n_i, sqmat1);
    SquareMatrix sqmat3(n_i, sqmat1);
    SquareMatrix sqmat4(n_i, sqmat1);
    SquareMatrix sqmat5(n_i, sqmat1);
    sqmat3+=sqmat2; // Addition
    sqmat4-=sqmat2; // Subtraction
    sqmat5*=sqmat2; // Multiplication
    SquareMatrix sqmat6(sqmat2.transpose()); // Transpose
    std::cout << "Testing matrix initialization..... ";
    sqmat2.print(std::cout) << std::endl;
    std::cout << "Testing matrix addition........... ";
    sqmat3.print(std::cout) << std::endl;
    std::cout << "Testing matrix subtraction........ ";
    sqmat4.print(std::cout) << std::endl;;
    std::cout << "Testing matrix multiplication..... ";
    sqmat5.print(std::cout) << std::endl;;
    std::cout << "Testing matrix transpose.......... ";
    sqmat6.print(std::cout) << std::endl;;
    REQUIRE( (sqmat2==sqmat3) == true ); // Equality test
    REQUIRE( sqmat2.toString() == "[[0,0,0],[1,1,1],[2,2,2]]" );
    REQUIRE( sqmat3.toString() == "[[0,0,0],[2,2,2],[4,4,4]]" );
    REQUIRE( sqmat4.toString() == "[[0,0,0],[0,0,0],[0,0,0]]" );
    REQUIRE( sqmat5.toString() == "[[0,0,0],[3,3,3],[6,6,6]]" );
    REQUIRE( sqmat6.toString() == "[[0,1,2],[0,1,2],[0,1,2]]" );
    t1.push_back(std::thread(std::bind(&SquareMatrix::toString, &sqmat2)));
    t1.push_back(std::thread(std::bind(&SquareMatrix::toString, &sqmat3)));
    t1.push_back(std::thread(std::bind(&SquareMatrix::toString, &sqmat4)));
    t1.push_back(std::thread(std::bind(&SquareMatrix::toString, &sqmat5)));
    for(auto& th : t1)
    {
        th.join();
    }
}

SquareMatrix::SquareMatrix()
{

}

SquareMatrix::SquareMatrix(int i_n, const std::vector<std::vector<IntElement>>& i_elements)
{
    if (i_elements[0].size() != i_elements.size())
    {
        std::cout << "Matrix is not square matrix";
    }
    if (i_n < 0)
    {
        std::cout << "Dimensions can't be negative";
    }
    else
    {
        n = i_n;
        elements = i_elements;
    }
}

SquareMatrix::SquareMatrix(const SquareMatrix& m)
{
    n = m.n;
    elements = m.elements;
}

SquareMatrix::~SquareMatrix()
{

}

SquareMatrix SquareMatrix::transpose()
{
    SquareMatrix tmp(n, elements);
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            tmp.elements[i][j] = this->elements[j][i];
        }
    }
    return tmp;
}

SquareMatrix& SquareMatrix::operator+=(const SquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j] += m.elements[i][j];
        }
    }
    return *this;
}

SquareMatrix& SquareMatrix::operator-=(const SquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j] -= m.elements[i][j];
        }
    }
    return *this;
}

SquareMatrix& SquareMatrix::operator*=(const SquareMatrix& m)
{
    if (this == &m)
        {
            return *this;
        }
    SquareMatrix tmp(n,elements);
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            tmp.elements[i][j] = 0;
        }
    }
    for (int i = 0;i < n;i++)
    {
        for (int j = 0;j < n;j++)
        {
            for(int k = 0;k < n;k++)
            {
                tmp.elements[i][j] += elements[k][j] * m.elements[i][k];
            }
        }
    }
    for (int i = 0;i < n;i++)
    {
        for(int j = 0;j < n;j++)
        {
            elements[i][j] = tmp.elements[i][j];
        }
    }
    return *this;
}

bool SquareMatrix::operator==(const SquareMatrix& m)
{
    return elements.size() == m.elements.size();
}

std::ostream& SquareMatrix::print(std::ostream& os)
{
    os << toString();
    return os;
}

std::string SquareMatrix::toString()
{
    std::stringstream os;
    os << "[";
    for (int i = 0; i < elements.size(); i++)
    {
        os << "[";
        for (int j = 0; j < elements.size(); j++)
        {
            os << elements[i][j];
            if (j < elements.size()-1)
            {
                os << ",";
            }
        }
        os << "]";
        if (i < elements.size()-1)
        {
                os << ",";
        }
    }
    os << "]";
    std::string result = os.str();
    std::cout << result << std::endl;
    return result;
}
